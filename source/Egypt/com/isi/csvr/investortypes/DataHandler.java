package com.isi.csvr.investortypes;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Feb 14, 2009
 * Time: 7:29:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class DataHandler {

    protected static final byte BUY = 0;
    protected static final byte SELL = 1;

    protected static ArrayList<InvestorInfo> extractDetailedInvestorInfoFromRowInfo(ArrayList<InvestorInfo> rowInfo) {

        double buyTotal = 0;
        double sellTotal = 0;
        for (InvestorInfo inv : rowInfo) {
            buyTotal += inv.getBuyTurnOver();
            sellTotal += inv.getSellTurnOver();
        }

        for (InvestorInfo inv : rowInfo) {
            double buyPct = inv.getBuyTurnOver() / buyTotal;
            double sellPct = inv.getSellTurnOver() / sellTotal;
            inv.setBuyPercentage(buyPct);
            inv.setSellPercentage(sellPct);
        }

        return rowInfo;
    }

    private static ArrayList<InvestorBarModel> getInvestorInfoForBuySell(ArrayList<InvestorInfo> list) {

        ArrayList<InvestorBarModel> result = new ArrayList<InvestorBarModel>();
        ArrayList<InvestorInfo> info = extractDetailedInvestorInfoFromRowInfo(list);

        for (InvestorInfo investorInfo : info) {
            result.add(new InvestorBarModel(investorInfo.getType(), BUY, investorInfo.getBuyPercentage() * 100));
        }

        for (InvestorInfo investorInfo : info) {
            result.add(new InvestorBarModel(investorInfo.getType(), SELL, investorInfo.getSellPercentage() * 100));
        }

        return result;
    }


    public static ArrayList<InvestorBarModel> getInvestorInfoForChart(ArrayList<InvestorInfo> basic) {

        ArrayList<InvestorInfo> result = DataHandler.extractDetailedInvestorInfoFromRowInfo(basic);
        return DataHandler.getInvestorInfoForBuySell(result);
    }
}
