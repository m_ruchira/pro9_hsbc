package com.isi.csvr.derivativepreference;

import com.isi.csvr.plugin.Plugin;
import com.isi.csvr.plugin.PluginStore;
import com.isi.csvr.plugin.event.PluginEvent;
import com.isi.csvr.shared.Meta;

/**
 * Created by IntelliJ IDEA.
 * User: hasika
 * Date: Jun 4, 2009
 * Time: 10:11:56 AM
 * To change this template use File | Settings | File Templates.
 */
public class DerivativePreferencePlugin implements Plugin {
    public final String ID = "DERIVATIVE_PREFERENCES";

    public DerivativePreferencePlugin() {
        PluginStore.putPlugin(ID, this);
    }

    public void load(Object... params) {
    }

    public void showUI(Object... params) {
        DerivativePreferenceUI.getSharedInstance().setVisible(true);
    }

    public void save(Object... params) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean isLoaded() {
        return true;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getCaption() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void unload(Object... params) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void pluginEvent(PluginEvent event, Object... data) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean isDataNeeded() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setData(int id, Object... params) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applyWorkspace(String id, String value) {
//To change body of implemented methods use File | Settings | File Templates.
    }

    public String getWorkspaceString(String id) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getViewSettingID() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getWindowTypeID() {
        return -1;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getWindowTypeCategory() {
        return Meta.WINDOW_TYPE_CATEGORY_INVALID;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
