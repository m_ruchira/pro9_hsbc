package com.mubasher.plugin;

import com.isi.csvr.table.Table;

import javax.swing.*;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Aug 21, 2007
 * Time: 3:14:04 PM
 */
public interface PFrameInterface {
    
    boolean isDetachable();

    boolean isDetached();

    void setDetached(boolean detached);

    void setDetachable(boolean detachableable);

    void setTable(Table table);

    void applySettings();

    void toggleTitleBar();

    void setTitleVisible(boolean setVisible);

    boolean isTitleVisible();

    int getWindowStyle();

    JTable getTable1();
}
