package com.mubasher.plugin;

import javax.swing.*;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Aug 21, 2007
 * Time: 3:17:53 PM
 */
public abstract class PFrame extends JInternalFrame implements PFrameInterface {

    public PFrame() {
    }

    public PFrame(String title) {
        super(title);
    }

    public PFrame(String title, boolean resizable) {
        super(title, resizable);
    }

    public PFrame(String title, boolean resizable, boolean closable) {
        super(title, resizable, closable);
    }

    public PFrame(String title, boolean resizable, boolean closable, boolean maximizable) {
        super(title, resizable, closable, maximizable);
    }

    public PFrame(String title, boolean resizable, boolean closable, boolean maximizable, boolean iconifiable) {
        super(title, resizable, closable, maximizable, iconifiable);
    }
}
