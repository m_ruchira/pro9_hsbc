package com.isi.csvr;

import com.isi.csvr.trade.Trade;

public class TNSTickerWrapper {
    private String message;
    private Trade g_oTrade;

    public TNSTickerWrapper(String message, Trade g_oTrade) {
        this.message = message;
        this.g_oTrade = g_oTrade;
    }

    public String getMessage() {
        return message;
    }

    public Trade getG_oTrade() {
        return g_oTrade;
    }

}
