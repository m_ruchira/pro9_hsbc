package com.isi.csvr.msgqueues;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class DataQueue {

    private List<MubasherMessage> list;

    /**
     * constructor.
     */
    public DataQueue() {
        list = Collections.synchronizedList(new LinkedList<MubasherMessage>());
    }

    /**
     * adding to queue.
     *
     * @param MubasherMessage data Object
     */
    public void add(MubasherMessage object) {
        list.add(object);
    }

    /**
     * @return MubasherMessage.
     */
    public MubasherMessage pop() {
        return list.remove(0);
    }

    /**
     * get size of queue.
     *
     * @return size
     */
    public int size() {
        return list.size();
    }

    /**
     * clear the queue.
     */
    public void clear() {
        list.clear();
    }
}
