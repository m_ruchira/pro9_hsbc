package com.isi.csvr.msgqueues;

import com.isi.csvr.ohlc.IntraDayOHLC;
import com.isi.csvr.ohlc.OHLCStore;
import com.isi.csvr.radarscreen.RadarScreenInterface;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.SharedMethods;

public class OHLCMessageHandler implements MessageQueueHandler {

    private DataQueue queue;
    private static OHLCMessageHandler self;

    private OHLCMessageHandler() {
        queue = new DataQueue();
        startQueueProcessor();
    }

    public static synchronized OHLCMessageHandler getSharedInstance() {
        if (self == null) {
            self = new OHLCMessageHandler();
        }
        return self;
    }

    public void addToTheQueue(MubasherMessage message) {
        queue.add(message);
    }

    public void startQueueProcessor() {
        new DataProcessor(this).startThread();
    }

    public String getDataProcessThreadName() {
        return "OHLCMessageHandler Queue";
    }

    public DataQueue getDataQueue() {
        return queue;
    }

    public void processFrame(MubasherMessage message) {
        IntraDayOHLC ohlcRecord = null;

        String symbol = message.getSymbol();
        String exchange = message.getExchange();
        int instrumentType = message.getInstrumentType();
        int decimalFactor = message.getDecimalFactor();
        String value = message.getMessageBody();

        if ((symbol != null) && (exchange != null)) {
            try {
                if (instrumentType == -1) {
                    instrumentType = Meta.INSTRUMENT_EQUITY;
                }
                ohlcRecord = OHLCStore.getInstance().getLastRecord(exchange, symbol, instrumentType);
                ohlcRecord.setData(exchange, instrumentType, value.split(Meta.FD), decimalFactor);
                OHLCStore.getInstance().addIntradayRecord(ohlcRecord);
                RadarScreenInterface.getInstance().addFrame(ohlcRecord, SharedMethods.getKey(exchange, symbol, instrumentType));
                ohlcRecord.setSnapShotUpdatedTime(System.currentTimeMillis());

                OHLCStore.getInstance().addOHLCRecord(ohlcRecord);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
