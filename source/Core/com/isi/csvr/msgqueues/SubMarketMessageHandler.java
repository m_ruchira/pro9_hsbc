package com.isi.csvr.msgqueues;

import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Market;
import com.isi.csvr.shared.Meta;

public class SubMarketMessageHandler implements MessageQueueHandler{

    private DataQueue queue;
    private static SubMarketMessageHandler self;

    private SubMarketMessageHandler() {
        queue = new DataQueue();
        startQueueProcessor();
    }

    public static synchronized SubMarketMessageHandler getSharedInstance() {
        if (self == null) {
            self = new SubMarketMessageHandler();
        }
        return self;
    }

    public void addToTheQueue(MubasherMessage message) {
        queue.add(message);
    }

    public void startQueueProcessor() {
        new DataProcessor(this).startThread();
    }

    public String getDataProcessThreadName() {
        return "SubMarketMessageHandler Queue";
    }

    public DataQueue getDataQueue() {
        return queue;
    }

    public void processFrame(MubasherMessage message) {
        Market subMarket;
        String symbol = message.getSymbol();
        String exchange = message.getExchange();
        int decimalFactor = message.getDecimalFactor();
        String value = message.getMessageBody();

        subMarket = ExchangeStore.getSharedInstance().getMarket(exchange, symbol);
        if (subMarket != null) {
            subMarket.setData(value.split(Meta.FD), decimalFactor);
        }
    }
}
