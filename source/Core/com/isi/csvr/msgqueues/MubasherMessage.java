package com.isi.csvr.msgqueues;

public class MubasherMessage {
    private String exchange;
    private String symbol;
    private String messageBody;
    private boolean snapShotFrame;
    private int instrumentType = -1;
    private int decimalFactor = 1;
    private String marketCenter = null;
    private boolean backlogFrame = false;
    private String exchangeCode;

    public MubasherMessage(String exchange, String symbol, String messageBody,boolean snapShotFrame,
                           int instrumentType, int decimalFactor,String marketCenter, boolean backlogFrame,
                           String exchangeCode) {
        this.exchange = exchange;
        this.symbol = symbol;
        this.messageBody = messageBody;
        this.snapShotFrame = snapShotFrame;
        this.instrumentType = instrumentType;
        this.decimalFactor = decimalFactor;
        this.marketCenter = marketCenter;
        this.backlogFrame = backlogFrame;
        this.exchangeCode = exchangeCode;
    }

    public String getExchange() {
        return exchange;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public boolean getSnapShotFrame() {
        return snapShotFrame;
    }

    public int getInstrumentType() {
        return instrumentType;
    }

    public int getDecimalFactor() {
        return decimalFactor;
    }

    public String getMarketCenter() {
        return marketCenter;
    }

    public boolean isBacklogFrame() {
        return backlogFrame;
    }

    public String getExchangeCode() {
        return exchangeCode;
    }
}
