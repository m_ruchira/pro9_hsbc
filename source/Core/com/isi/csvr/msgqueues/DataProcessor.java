package com.isi.csvr.msgqueues;

public class DataProcessor implements Runnable {

    private MessageQueueHandler messageQueueHandler;
    private final int frameSize = 2000;
    private boolean isActive = true;

    private String dataProcessThreadName;


    public DataProcessor(MessageQueueHandler dataStore) {
        this.messageQueueHandler = dataStore;
        this.dataProcessThreadName = dataStore.getDataProcessThreadName();
    }

    public void startThread() {
        Thread t = new Thread(this, dataProcessThreadName + " Thread Start");
        t.start();
    }

    public void run() {
        while (isActive) {
            DataQueue queue = messageQueueHandler.getDataQueue();
            int queueSize = queue.size();
            try {
                if (queueSize > 0) {
                    if (queueSize >= frameSize) {
                        queueSize = frameSize;
                    }
                    for (int i = 0; i < queueSize; i++) {
                        MubasherMessage mubasherMessage = queue.pop();
                        messageQueueHandler.processFrame(mubasherMessage);

                    }
                    sleepThread(10);

                } else {
                    // sleep
                    sleepThread(100);
                }

            } catch (Exception e) {
                sleepThread(100);
            }
        }
    }

    private void sleepThread(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
        }
    }
}
