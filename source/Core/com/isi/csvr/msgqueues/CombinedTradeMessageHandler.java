package com.isi.csvr.msgqueues;

import com.isi.csvr.ExchangeMap;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.ticker.SharedSettings;
import com.isi.csvr.ticker.advanced.*;
import com.isi.csvr.ticker.custom.TickerObject;
import com.isi.csvr.ticker.custom.TradeFeeder;
import com.isi.csvr.trade.CompanyTradeStore;
import com.isi.csvr.trade.Trade;
import com.isi.csvr.trade.TradeStore;

import java.util.ArrayList;

public class CombinedTradeMessageHandler implements MessageQueueHandler {

    private DataQueue queue;
    private static CombinedTradeMessageHandler self;

    private CombinedTradeMessageHandler() {
        queue = new DataQueue();
        startQueueProcessor();
    }

    public static synchronized CombinedTradeMessageHandler getSharedInstance() {
        if (self == null) {
            self = new CombinedTradeMessageHandler();
        }
        return self;
    }

    public void addToTheQueue(MubasherMessage message) {
        queue.add(message);
    }

    public void startQueueProcessor() {
        new DataProcessor(this).startThread();
    }

    public String getDataProcessThreadName() {
        return "CombinedTradeMessageHandler Queue";
    }

    public DataQueue getDataQueue() {
        return queue;
    }

    public void processFrame(MubasherMessage message) {

        Trade g_oTrade = null;
        String symbol = message.getSymbol();
        String exchange = message.getExchange();
        int instrumentType = message.getInstrumentType();
        int decimalFactor = message.getDecimalFactor();
        String value = message.getMessageBody();
        boolean snapshotFrame = message.getSnapShotFrame();
        String marketCenter = message.getMarketCenter();
        boolean backlogFrame = message.isBacklogFrame();
        String exchangeCode = message.getExchangeCode();

        if ((symbol != null) &&  (exchange != null)) {
            try {
                if (instrumentType == -1) {
                    instrumentType = Meta.INSTRUMENT_QUOTE;
                }
                g_oTrade = CompanyTradeStore.getSharedInstance().getLastTrade(exchange, symbol, instrumentType, backlogFrame);

                if (g_oTrade == null) {
                    try {
                        g_oTrade = new Trade(exchangeCode, symbol, instrumentType);
                    } catch (Exception e) {
                        g_oTrade = new Trade(ExchangeMap.getIDFor(exchange), symbol, instrumentType);
                    }
                }
                g_oTrade.setData(value.split(Meta.FD), decimalFactor);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if ((ExchangeStore.getSharedInstance().getExchange(exchange)).isTimensalesEnabled()) {

            boolean bVaidTrade = TradeStore.addTrade(g_oTrade, backlogFrame);
            if (bVaidTrade) {
                g_oTrade.setSnapShotUpdatedTime(System.currentTimeMillis());
            }

            if (SharedSettings.IS_CUSTOM_SELECTED) {
                if (ExchangeStore.isValidIinformationType(exchange, Meta.IT_TradeTicker)) {   //
                    if (ExchangeStore.isValidIinformationType(exchange, Meta.IT_MarketTimeAndSales)) {
//                                            if ((ExchangeStore.getSelectedExchangeID() != null) && ((ExchangeStore.getSelectedExchangeID().equals(exchange)) || TradeFeeder.getMode() == TWTypes.TickerFilter.MODE_WATCHLIST)) {
                        if ((ExchangeStore.getSelectedExchangeID() != null) && (ExchangeStore.getSelectedExchangeID().equals(exchange))) {
                            if ((!snapshotFrame) && (!backlogFrame) && (!Settings.isShowSummaryTicker())) {
                                //if (Settings.isTradeEnabledMode() && (!Settings.isShowSummaryTicker())) {
                                if ((!g_oTrade.isIndexType()) && (bVaidTrade) && TradeFeeder.isVisible()) {
                                    //if (TradeFeeder.isPreferredExchange(exchange)) {
                                    TickerObject oTickerData = new TickerObject();
                                    int tickerStatus;
                                    if ((g_oTrade != null)) {
                                        if (g_oTrade.getOddlot() == 1)
                                            tickerStatus = Constants.TICKER_STATUS_SMALLTRADE;
                                        else if (g_oTrade.getNetChange() == 0)
                                            tickerStatus = Constants.TICKER_STATUS_NOCHANGE;
                                        else if (g_oTrade.getNetChange() > 0)
                                            tickerStatus = Constants.TICKER_STATUS_UP;
                                        else
                                            tickerStatus = Constants.TICKER_STATUS_DOWN;

                                        oTickerData.setData(exchange, symbol, g_oTrade.getTradePrice(),
                                                g_oTrade.getTradeQuantity(), g_oTrade.getNetChange(),
                                                g_oTrade.getPrecentNetChange(), tickerStatus, g_oTrade.getSplits(), instrumentType);//
                                        TradeFeeder.addData(oTickerData);

                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                if ((!snapshotFrame) && (!backlogFrame) && ((!g_oTrade.isIndexType()) && (bVaidTrade))) {

                    ArrayList<String> upperexchangeList = UpperPanelSettings.getExchangeList();
                    if (upperexchangeList.contains(exchange)) /*|| UpperFilterPanel.mode == TWTypes.TickerFilter.MODE_WATCHLIST)*/ {
                        if (ExchangeStore.isValidIinformationType(exchange, Meta.IT_TradeTicker)) {
                            if (ExchangeStore.isValidIinformationType(exchange, Meta.IT_MarketTimeAndSales)) {
                                if ((!UpperPanelSettings.showSummaryTicker) && UpperTickerFeeder.isVisible()) {
                                    if (UpperTickerFeeder.isExchangeModeSelected || UpperTickerFeeder.isWatchListModeSelected) {
                                        CommonTickerObject upperTickerOb = new CommonTickerObject();
                                        upperTickerOb.setData(exchange, symbol, g_oTrade.getTradePrice(),
                                                g_oTrade.getTradeQuantity(), g_oTrade.getNetChange(),
                                                g_oTrade.getPrecentNetChange(), 0
                                                , g_oTrade.getSplits(), instrumentType);//
                                        UpperTickerFeeder.addData(upperTickerOb);
                                    }
                                }
                            }
                        }
                    }


                    ArrayList<String> middleexchangeList = MiddlePanelSettings.getExchangeList();
                    if (middleexchangeList.contains(exchange)) /*|| MiddleFilterPanel.mode == TWTypes.TickerFilter.MODE_WATCHLIST)*/ {
                        if (ExchangeStore.isValidIinformationType(exchange, Meta.IT_TradeTicker)) {
                            if (ExchangeStore.isValidIinformationType(exchange, Meta.IT_MarketTimeAndSales)) {
                                if ((!MiddlePanelSettings.showSummaryTicker) && MiddleTickerFeeder.isVisible()) {
                                    if (MiddleTickerFeeder.isExchangeModeSelected || MiddleTickerFeeder.isWatchListModeSelected) {
                                        CommonTickerObject upperTickerOb = new CommonTickerObject();
                                        upperTickerOb.setData(exchange, symbol, g_oTrade.getTradePrice(),
                                                g_oTrade.getTradeQuantity(), g_oTrade.getNetChange(),
                                                g_oTrade.getPrecentNetChange(), 0
                                                , g_oTrade.getSplits(), instrumentType);//
                                        MiddleTickerFeeder.addData(upperTickerOb);
                                    }
                                }
                            }
                        }
                    }

                    ArrayList<String> lowerexchangeList = LowerPanelSettings.getExchangeList();
                    if (lowerexchangeList.contains(exchange))/*|| LowerFilterPanel.mode == TWTypes.TickerFilter.MODE_WATCHLIST)*/ {
                        if (ExchangeStore.isValidIinformationType(exchange, Meta.IT_TradeTicker)) {
                            if (ExchangeStore.isValidIinformationType(exchange, Meta.IT_MarketTimeAndSales)) {
                                if ((!LowerPanelSettings.showSummaryTicker) && LowerTickerFeeder.isVisible()) {
                                    if (LowerTickerFeeder.isExchangeModeSelected || LowerTickerFeeder.isWatchListModeSelected) {
                                        CommonTickerObject upperTickerOb = new CommonTickerObject();
                                        upperTickerOb.setData(exchange, symbol, g_oTrade.getTradePrice(),
                                                g_oTrade.getTradeQuantity(), g_oTrade.getNetChange(),
                                                g_oTrade.getPrecentNetChange(), 0
                                                , g_oTrade.getSplits(), instrumentType);//
                                        LowerTickerFeeder.addData(upperTickerOb);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
