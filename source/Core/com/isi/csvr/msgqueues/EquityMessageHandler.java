package com.isi.csvr.msgqueues;

import com.isi.csvr.MapTodaysTrades;
import com.isi.csvr.bandwidthoptimizer.OptimizedTreeRenderer;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.forex.FXObject;
import com.isi.csvr.forex.ForexStore;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.shared.UpdateNotifier;

public class EquityMessageHandler implements MessageQueueHandler {

    private DataQueue queue;
    private static EquityMessageHandler self;
    private MapTodaysTrades g_oTodaysTrades;

    private EquityMessageHandler(MapTodaysTrades g_oTodaysTrades) {
        queue = new DataQueue();
        this.g_oTodaysTrades = g_oTodaysTrades;
        startQueueProcessor();
    }

    public static synchronized EquityMessageHandler getSharedInstance(MapTodaysTrades g_oTodaysTrades) {
        if (self == null) {
            self = new EquityMessageHandler(g_oTodaysTrades);
        }
        return self;
    }

    public void addToTheQueue(MubasherMessage message) {
        queue.add(message);
    }

    public void startQueueProcessor() {
        new DataProcessor(this).startThread();
    }

    public String getDataProcessThreadName() {
        return "EquityMessageHandler Queue";
    }

    public DataQueue getDataQueue() {
        return queue;
    }

    public void processFrame(MubasherMessage message) {
        String symbol = message.getSymbol();
        String exchange = message.getExchange();
        int instrumentType = message.getInstrumentType();
        int decimalFactor = message.getDecimalFactor();
        String value = message.getMessageBody();
        boolean snapshotFrame = message.getSnapShotFrame();
        String marketCenter = message.getMarketCenter();
        FXObject fxStock = null;
        Stock oStock = null;
        double g_dLastTradePrice = 0;

        try {
            if ((symbol != null) && (exchange != null)) {
                if (instrumentType < 0) {
                    instrumentType = Meta.INSTRUMENT_EQUITY;
                }
                if (instrumentType == Meta.INSTRUMENT_FOREX) {
                    fxStock = ForexStore.getSharedInstance().getForexObject(exchange, symbol, instrumentType);
                    long blockSize = fxStock.setData(value.split(Meta.FD), decimalFactor, snapshotFrame);
                    if (!fxStock.isSmallestBlockSize(blockSize)) {
                        return;
                    }
                }
                if (marketCenter != null) {
                    symbol = symbol + "." + marketCenter;
                }

                //  ---- Shanika ---------
                boolean isStockNot = false;
                Stock st = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrumentType);
                if (st == null) {
                    isStockNot = true;

                }

                oStock = DataStore.getSharedInstance().createStockObject(exchange, symbol, instrumentType);
                oStock.setData(value/*.split(Meta.FD)*/, decimalFactor, snapshotFrame);
                oStock.setSnapShotUpdatedTime(System.currentTimeMillis());


                if ((oStock.getSectorCode().equals("") || oStock.getSectorCode().equals("N/A") || oStock.getSectorCode().equals("0")) && (oStock.getInstrumentType() != 7)) {
                    oStock.setSectorCode(Constants.DEFAULT_SECTOR);
                }
                Exchange ex = ExchangeStore.getSharedInstance().getExchange(exchange);

                try {
                    if (isStockNot && ex.isExchageMode()) {
                        OptimizedTreeRenderer.selectedSymbols.put(oStock.getKey(), true);
                        oStock.setSymbolEnabled(true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                oStock.print();
                g_dLastTradePrice = oStock.getLastTradeValue();

                if (g_dLastTradePrice > 0F) {
                    g_oTodaysTrades.insertSymbol(exchange, symbol.trim(), oStock.getMarketID(), instrumentType);
                }

                UpdateNotifier.setShapshotUpdated();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
