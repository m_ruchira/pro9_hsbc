package com.isi.csvr.msgqueues;

public interface MessageQueueHandler {

    public void addToTheQueue(MubasherMessage message);

    public void startQueueProcessor();

    public String getDataProcessThreadName();

    public DataQueue getDataQueue();

    public void processFrame(MubasherMessage message);
}
