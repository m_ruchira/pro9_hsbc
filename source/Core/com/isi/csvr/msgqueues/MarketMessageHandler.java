package com.isi.csvr.msgqueues;

import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.Meta;

public class MarketMessageHandler implements MessageQueueHandler{

    private DataQueue queue;
    private static MarketMessageHandler self;

    private MarketMessageHandler() {
        queue = new DataQueue();
        startQueueProcessor();
    }

    public static synchronized MarketMessageHandler getSharedInstance() {
        if (self == null) {
            self = new MarketMessageHandler();
        }
        return self;
    }

    public void addToTheQueue(MubasherMessage message) {
        queue.add(message);
    }

    public void startQueueProcessor() {
        new DataProcessor(this).startThread();
    }

    public String getDataProcessThreadName() {
        return "MarketMessageHandler Queue";
    }

    public DataQueue getDataQueue() {
        return queue;
    }

    public void processFrame(MubasherMessage message) {
        Exchange exchangeObject;
        String exchange = message.getExchange();
        int decimalFactor = message.getDecimalFactor();
        String value = message.getMessageBody();

        try {
            exchangeObject = ExchangeStore.getSharedInstance().getExchange(exchange);
            exchangeObject.setData(value.split(Meta.FD), decimalFactor);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
