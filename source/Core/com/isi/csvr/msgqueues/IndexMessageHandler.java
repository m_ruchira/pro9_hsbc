package com.isi.csvr.msgqueues;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Stock;

public class IndexMessageHandler implements MessageQueueHandler{

    private DataQueue queue;
    private static IndexMessageHandler self;

    private IndexMessageHandler() {
        queue = new DataQueue();
        startQueueProcessor();
    }

    public static synchronized IndexMessageHandler getSharedInstance() {
        if (self == null) {
            self = new IndexMessageHandler();
        }
        return self;
    }

    public void addToTheQueue(MubasherMessage message) {
        queue.add(message);
    }

    public void startQueueProcessor() {
        new DataProcessor(this).startThread();
    }

    public String getDataProcessThreadName() {
        return "IndexMessageHandler Queue";
    }

    public DataQueue getDataQueue() {
        return queue;
    }

    public void processFrame(MubasherMessage message) {
        String symbol = message.getSymbol();
        String exchange = message.getExchange();
        int instrumentType = message.getInstrumentType();
        int decimalFactor = message.getDecimalFactor();
        String value = message.getMessageBody();
        Stock oStock = null;
        double g_dLastTradePrice = 0;
        boolean snapshotFrame = message.getSnapShotFrame();

        if ((symbol != null) && (exchange != null)) {
            try {
                if (instrumentType == -1) {
                    instrumentType = Meta.INSTRUMENT_INDEX;
                }
                oStock = DataStore.getSharedInstance().createStockObject(exchange, symbol, instrumentType);
                oStock.setData(value/*.split(Meta.FD)*/, decimalFactor, snapshotFrame);
                oStock.setSnapShotUpdatedTime(System.currentTimeMillis());
                g_dLastTradePrice = oStock.getLastTradeValue();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
