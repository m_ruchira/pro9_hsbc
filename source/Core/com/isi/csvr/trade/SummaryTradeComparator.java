package com.isi.csvr.trade;

import java.util.Comparator;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class SummaryTradeComparator implements Comparator {

    public SummaryTradeComparator() {
    }

    public int compare(Object o1, Object o2) {
        SummaryTrade h1 = (SummaryTrade) o1;
        SummaryTrade h2 = (SummaryTrade) o2;

        if (h1.getTradePrice() > h2.getTradePrice())
            return 1;
        else if (h1.getTradePrice() < h2.getTradePrice())
            return -1;
        else
            return 0;
    }

    public boolean equals(Object obj) {
        return super.equals(obj);
    }

}