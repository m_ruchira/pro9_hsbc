package com.isi.csvr.debug;

import com.dfn.mtr.mix.beans.MIXObject;
import com.dfn.mtr.mix.sabb.beans.MIXExtendedObject;
import com.isi.csvr.Client;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.UnicodeUtils;

import javax.swing.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Calendar;

/**
 * Created by IntelliJ IDEA.
 * User: shanakak
 * Date: Jul 26, 2012
 * Time: 1:11:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class ResponseFileWriter {
    private static ResponseFileWriter self;

    private ResponseFileWriter() {
    }

    public static synchronized ResponseFileWriter getSharedInstance() {
        if (self == null) {
            self = new ResponseFileWriter();
        }
        return self;
    }

    public void writeToFileRQ(MIXObject response, Byte path) {
        try {
            String write = "[Time: %s] [Path is: %s] Response: %s\n";
            // Create file
            Calendar c = Calendar.getInstance();

            FileWriter fstream = new FileWriter(System.getProperties().get("user.dir")+"//receive_out_pro.txt",true);
//            FileWriter fstream = new FileWriter("D://receive_out_pro.txt",true);
//            FileWriter fstream = new FileWriter("/System/out.txt",true);
            BufferedWriter out = new BufferedWriter(fstream);
            out.write(String.format(write,c.getTime().toString(),path,response.toString()));
//            out.write("hiiiiii");
            //Close the output stream
            out.close();
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
    }

    public void writeToFileReceiveQueue(MIXObject resObject, Byte path) {
        writeToFileRQ(resObject, path);
    }

    public void writeToFileSQ(String response, Byte path) {
        try {
            String write = "[Time: %s] [Path is: %s] Request: %s\n";
            // Create file
            Calendar c = Calendar.getInstance();

//            FileWriter fstream = new FileWriter(*//*System.getProperties().get("user.dir") +*//*"D://send_out_pro.txt",true);
            FileWriter fstream = new FileWriter(System.getProperties().get("user.dir") +"//send_out_pro.txt",true);
//            FileWriter fstream = new FileWriter("/System/out.txt",true);
            BufferedWriter out = new BufferedWriter(fstream);
            out.write(String.format(write,c.getTime().toString(),path,response));
            //Close the output stream
            out.close();
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
    }

    public void writeToFileSendQueue(String resObject, Byte path) {
        writeToFileSQ(resObject, path);
    }

    public void writeToSocketStatus(String resObject) {
//        writeToFileSocketStatus(resObject);
    }

    public void writeToFileSocketStatus(String response) {
        try {
            String write = "[Time: %s] Request: %s\n";
            // Create file
            Calendar c = Calendar.getInstance();

            FileWriter fstream = new FileWriter(System.getProperties().get("user.dir")+ "//socket_pro.txt",true);
//            FileWriter fstream = new FileWriter("/System/out.txt",true);
            BufferedWriter out = new BufferedWriter(fstream);
            out.write(String.format(write,c.getTime().toString(),response));
            //Close the output stream
            out.close();
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
    }

    /*public static void main(String[] args) {
//        Dear Customer,\n We have noticed that you did not input the Verification code within the last 5 Minutes.\n Therefore, for your security you will need to log-on again
        String errMsg = "<HTML>Dear Customer,<BR>We have noticed that you did not input the Verification code within the last 5 Minutes.<BR>Therefore, for your security you will need to log-on again</HTML>|<HTML>Dear Customer,<BR>We have noticed that you did not input the Verification code within the last 5 Minutes.<BR>Therefore, for your security you will need to log-on again</HTML>";
        showInvalidAuthMessage(getNativeString(getLanguageSpecificString(errMsg)));
    }

    public static void showMsg(String err){
        *//**//*if (err != null) {
            Object[] options = {
                "OK"};
        JOptionPane.showOptionDialog(null,
                err,
                "Error",
                JOptionPane.OK_OPTION,
                JOptionPane.ERROR_MESSAGE,
                null,
                options,
                options[0]);
        }*//**//*
    }

    public static String getNativeString(String sUnicode) {
        int i = 0;
        int buffindex = 0;
        char[] buf = new char[sUnicode.length()];
        int iLen = 0;
        char ch;
        char next;

        iLen = sUnicode.length();

        while (i < iLen) {
            ch = getNext(sUnicode, i++);
            if (ch == '\\') {
                if ((next = getNext(sUnicode, i++)) == 'u') {
                    if ((iLen - i) >= 4) {
                        buf[buffindex++] = processUnicode(sUnicode.substring(i, i + 4));
                        i += 4;
                    } else {
                        buf[buffindex++] = '\\';
                        buf[buffindex++] = 'u';
                        while (i < iLen) buf[buffindex++] = sUnicode.charAt(i++);
                        i = iLen;
                    }
                } else if (next == -1) {
                    return (new String(buf, 0, buffindex));
                } else {
                    buf[buffindex++] = '\\';
                    i--;
                    //buf[buffindex++] = next;
                }
            } else {
                buf[buffindex++] = ch;
            }
        }
        return (new String(buf, 0, buffindex));
    }

    public static String getLanguageSpecificString(String sValue) {
        return getLanguageSpecificString(sValue, "|");
    }

    public static String getLanguageSpecificString(String sValue, String sSeperator) {
        String sEngValue = "";
        String sLangValue = "";

        try {
            String[] langs = sValue.split("\\"+sSeperator);
            sEngValue = langs[-1];
            sLangValue = UnicodeUtils.getNativeString(langs[-1]);
                return sLangValue;
        } catch (Exception e) {
            return sEngValue;
        }

    }

    public static void showInvalidAuthMessage(String message) {
        Object[] options = {
                "OK"};
        JOptionPane.showOptionDialog(null,
                message,
                "Error",
                JOptionPane.OK_OPTION,
                JOptionPane.ERROR_MESSAGE,
                null,
                options,
                options[0]);
    }


    private static char getNext(String sUnicode, int i) {
        if (i < sUnicode.length())
            return sUnicode.charAt(i);
        else
            return (char) -1;
    }

    *//**//*
    * Converts the 4 digit code to the native char
    *//**//*
    private static char processUnicode(String sUnicode) {
        char ch;
        int d = 0;
        loop:
        for (int i = 0; i < 4; i++) {
            ch = sUnicode.charAt(i);
            switch (ch) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    d = (d << 4) + ch - '0';
                    break;
                case 'a':
                case 'b':
                case 'c':
                case 'd':
                case 'e':
                case 'f':
                    d = (d << 4) + 10 + ch - 'a';
                    break;
                case 'A':
                case 'B':
                case 'C':
                case 'D':
                case 'E':
                case 'F':
                    d = (d << 4) + 10 + ch - 'A';
                    break;
                default:
                    break loop;
            }
        }
        return (char) d;
    }*//*


*/

}
