package com.isi.csvr;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Feb 11, 2009
 * Time: 9:51:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class BarInfo {

    private long quantity;
    private double price;

    public BarInfo(double price, long quantity) {
        this.quantity = quantity;
        this.price = price;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
