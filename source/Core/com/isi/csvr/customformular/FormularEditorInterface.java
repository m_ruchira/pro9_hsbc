package com.isi.csvr.customformular;

import com.isi.csvr.shared.FunctionStockInterface;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Aug 22, 2006
 * Time: 11:24:07 AM
 * To change this template use File | Settings | File Templates.
 */
public interface FormularEditorInterface {

    double getValue(FunctionStockInterface stock);

    String getColumnName();

    String getFunctionString();

}
