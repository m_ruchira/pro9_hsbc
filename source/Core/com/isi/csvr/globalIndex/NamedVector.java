package com.isi.csvr.globalIndex;

import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 25-Mar-2008 Time: 10:42:18 To change this template use File | Settings
 * | File Templates.
 */
class NamedVector extends Vector implements Comparable {
    String name;

    public NamedVector(String name) {
        this.name = name;
    }

    public NamedVector(String name, Hashtable hash) {
        this.name = name;
        Enumeration en = hash.elements();
        while (en.hasMoreElements()) {
            add(en.nextElement());

        }
        Collections.sort(this);
//       Collections.sort(this);
    }

    public NamedVector(String name, Object elements[]) {
        this.name = name;
        for (int i = 0, n = elements.length; i < n; i++) {
            add(elements[i]);
        }
        Collections.sort(this);
    }

    public String toString() {
        return name;
    }

    public int compareTo(Object o) {
        return toString().compareTo(o.toString());
    }

}
