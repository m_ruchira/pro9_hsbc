package com.isi.csvr.scanner.scannerGUIs;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 24, 2008
 * Time: 5:44:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class TreeInfo {

    private String name = null;
    private int scanType = 0;

    public TreeInfo(String name, int type) {
        this.name = name;
        this.scanType = type;

    }

    public String toString() {
        return name;
    }

    public int getType() {
        return scanType;
    }
}
