package com.isi.csvr.scanner.Results;

import com.isi.csvr.scanner.ScanPoint;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 18, 2008
 * Time: 8:07:12 PM
 * To change this template use File | Settings | File Templates.
 */


public class ResultBase  /*extends  IComparable*/ {
//        private Company company;
    //        public Company getCompany {
    //            get { return company; }
    //        }
    private String company;




    public String getSExchangeName() {
        return sExchangeName;
    }

    public void setSExchangeName(String sExchangeName) {
        this.sExchangeName = sExchangeName;
    }

    private String sExchangeName = "";

    public String getCompany() {
        return company;
    }

    public void setCompany(String value) {
        company = value;
    }

    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String value) {
        key = value;
    }


    private String companyName;

    public void setCompanyName(String value) {
        companyName = value;
    }

    public String getCompanyName() {
        return companyName;
    }

    private double last;

    public void setLast(double value) {
        open = value;

    }

    public double getLast() {
        return open;
    }

    private double change;

    public void setChange(double value) {
        change = value;
    }

    public double getChange() {
        return change;
    }

    private double percentChange;

    public double getPercentChange() {
        return percentChange;

    }

    public void setPercentChange(double value) {
        percentChange = value;
    }

    private double open;

    public void setOpen(double value) {
        open = value;
    }

    public double getOpen() {
        return open;
    }

    private double high;

    public void setHigh(double value) {
        high = value;
    }

    public double getHigh() {
        return high;

    }

    private double low;

    public void setLow(double value) {
        low = value;
    }

    public double getLow() {
        return low;
    }


    public Date timeHigh52;



    public Date timeLow52;

    public Date getTimeHigh52() {
        return timeHigh52;
    }

    public void setTimeHigh52(Date timeHigh52) {
        this.timeHigh52 = timeHigh52;
    }

    public Date getTimeLow52() {
        return timeLow52;
    }

    public void setTimeLow52(Date timeLow52) {
        this.timeLow52 = timeLow52;
    }

    public ResultBase() {
    }

    public ResultBase(String company, ScanPoint sP) {
        this.company = company;
//            this.key = company.Key;
        this.key = "";
//            this.companyName = company.getLongName();
        this.companyName = "";
        this.last = sP.Close;
        this.change = sP.Change;
        this.percentChange = sP.PercentChange;
        this.open = sP.Open;
        this.high = sP.High;
        this.low = sP.Low;


    }

     //--------------- Added by Shanika--- for 52High and 52Low

     public ResultBase(String cmpny, ScanPoint sPLast, Date week52highTime) {
          this.company = cmpny;
//            this.key = company.Key;
        this.key = "";
//            this.companyName = company.getLongName();
        this.companyName = "";
        this.last = sPLast.Close;
        this.change = sPLast.Change;
        this.percentChange = sPLast.PercentChange;
        this.open = sPLast.Open;
        this.high = sPLast.High;
        this.low = sPLast.Low;

        this.timeHigh52 =week52highTime;
         this.timeLow52 = week52highTime;
    }





    //






     public ResultBase(String cmpny, ScanPoint sPLast, String week52highTime) {
          this.company = cmpny;
//            this.key = company.Key;
        this.key = "";
//            this.companyName = company.getLongName();
        this.companyName = "";
        this.last = sPLast.Close;
        this.change = sPLast.Change;
        this.percentChange = sPLast.PercentChange;
        this.open = sPLast.Open;
        this.high = sPLast.High;
        this.low = sPLast.Low;

        this.timeStringHigh52 =week52highTime;
         this.timeStringLow52 = week52highTime;
    }

    private String timeStringHigh52;
    private String timeStringLow52;

    public String getTimeStringHigh52() {
        return timeStringHigh52;
    }

    public void setTimeStringHigh52(String timeStringHigh52) {
        this.timeStringHigh52 = timeStringHigh52;
    }

    public String getTimeStringLow52() {
        return timeStringLow52;
    }

    public void setTimeStringLow52(String timeStringLow52) {
        this.timeStringLow52 = timeStringLow52;
    }
    //todo activate compartor
//    protected int compare(Object obj) {
//        // this is done opposite way to sort descending
//        return  new Double(Math.abs(((ResultBase) obj).percentChange)).compareTo(new Double(Math.abs(this.percentChange)));
//    }


     public ResultBase(String cmpny, ScanPoint sPLast, long week52highTime) {
         this.company = cmpny;
//            this.key = company.Key;
        this.key = "";
//            this.companyName = company.getLongName();
        this.companyName = "";
        this.last = sPLast.Close;
        this.change = sPLast.Change;
        this.percentChange = sPLast.PercentChange;
        this.open = sPLast.Open;
        this.high = sPLast.High;
        this.low = sPLast.Low;

        this.high52 =week52highTime;
         this.low52 = week52highTime;
    }

    private long low52;
    private long high52;

    public long getLow52() {
    return low52;
}

    public void setLow52(long low52) {
        this.low52 = low52;
    }

    public long getHigh52() {
        return high52;
    }

    public void setHigh52(long high52) {
        this.high52 = high52;
    }
}

