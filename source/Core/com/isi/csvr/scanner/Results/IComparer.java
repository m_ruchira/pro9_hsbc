package com.isi.csvr.scanner.Results;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 27, 2008
 * Time: 9:37:18 PM
 * To change this template use File | Settings | File Templates.
 */
public interface IComparer {

    int Compare(Object x, Object y);
}
