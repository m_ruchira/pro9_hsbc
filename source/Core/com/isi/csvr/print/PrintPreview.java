package com.isi.csvr.print;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.print.*;
import java.util.*;

import com.isi.util.*;
import com.isi.csvr.shared.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

public class PrintPreview
        extends JFrame implements WindowListener {
    //Page previewPanel;
    BufferedImage img;

    public PrintPreview(Printable target, String title) {
        super();

        /*int pageIndex  =0;
        setSize(200,400);
        show();
        PageFormat pageFormat = Settings.getPageFormat();

        JPanel contentPane = new JPanel();
        contentPane.setLayout(new FlowLayout());
        JScrollPane scrollPane = new JScrollPane(contentPane);
        getContentPane().add(scrollPane);
        
        getContentPane().setLayout(new FlowLayout());
        addWindowListener(this);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        while (true){
            img = new BufferedImage((int)pageFormat.getWidth(), (int)pageFormat.getHeight(), 
                BufferedImage.TYPE_BYTE_GRAY);
            Graphics g = img.getGraphics();
            g.setColor(Color.white);
            g.fillRect(0, 0, (int)pageFormat.getWidth(), (int)pageFormat.getHeight()); 
            try{
                if (target.print(g,pageFormat,pageIndex) == Printable.NO_SUCH_PAGE)
                    return;
                pageIndex++;
            }catch(Exception e){
                e.printStackTrace();
            }
            previewPanel = new Page((Image)img);
            System.out.println("> " + pageIndex);
            previewPanel.setPreferredSize(new Dimension((int)pageFormat.getWidth(), (int)pageFormat.getHeight()));
            contentPane.add(previewPanel);
            //System.gc();
            //System.gc();
        }
        
        //show();*/

        Dimension d = null;

        try {
            PrinterJob prnJob = PrinterJob.getPrinterJob();
            prnJob.setPrintable(target, Settings.getPageFormat());
            if (!prnJob.printDialog()) {
                return;
            }

            setCursor(Cursor.getPredefinedCursor(
                    Cursor.WAIT_CURSOR));
            prnJob.print();
            d = null;
            setCursor(Cursor.getPredefinedCursor(
                    Cursor.DEFAULT_CURSOR));

        } catch (PrinterException ex) {
            ex.printStackTrace();
            System.err.println("Printing error: " + ex.toString());
        }
        setTitle(Language.getString("PRINT_PREVIEW"));
        setResizable(false);
        //enableDoubleBuffering(table);
    }

    public void windowOpened(WindowEvent e) {
    }

    public void windowClosing(WindowEvent e) {
        System.out.println("Removing ");
        Component components[] = getContentPane().getComponents();
        getContentPane().removeAll();
        Arrays.fill(components, null);
        components = null;
        System.gc();
        System.gc();
        //System.gc();
        //System.gc();
        //System.gc();
    }

    public void windowClosed(WindowEvent e) {
    }

    public void windowIconified(WindowEvent e) {
    }

    public void windowDeiconified(WindowEvent e) {
    }

    public void windowActivated(WindowEvent e) {
    }

    public void windowDeactivated(WindowEvent e) {
    }

    public static void disableDoubleBuffering(Component c) {
        RepaintManager currentManager = RepaintManager.currentManager(c);
        currentManager.setDoubleBufferingEnabled(false);
    }

    /**
     * Re-enables double buffering globally.
     */
    public static void enableDoubleBuffering(Component c) {
        RepaintManager currentManager = RepaintManager.currentManager(c);
        currentManager.setDoubleBufferingEnabled(true);
    }
}