package com.isi.csvr.shared;

import java.util.LinkedList;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class SafeList {


    LinkedList list;

    public SafeList() {
        list = new LinkedList();
    }

    /**
     * @param location location to be inserted
     */
    public synchronized void insert(int location, Object order) {
        try {
            list.add(location, order);
        } catch (Exception ex) {
            System.out.println("Error while inserting");
            ex.printStackTrace();
        }

    }

    /**
     * Append the given object to the end of the list
     *
     * @param order order to be added to the list
     */
    public synchronized void add(Object order) {
        list.addLast(order);
    }

    /**
     * Remove the pointed object from the list
     *
     * @param location of the order to be removed
     */
    public synchronized void remove(int location) {
        try {
            list.remove(location);
        } catch (Exception ex) {
            System.out.println("Error while removing");
            ex.printStackTrace();
        }

    }

    /**
     * return the object
     *
     * @param location of the object to be returned
     * @return object if found or null
     */
    public synchronized Object get(int location) {
        try {
            return list.get(location);
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Replace the give object in the lit
     *
     * @param location of the object to be replaced
     * @param object   to be replaced
     */
    public synchronized void set(int location, Object object) {
        try {
            list.set(location, object);
        } catch (Exception ex) {
            System.out.println("Error while setting the object");
            ex.printStackTrace();
        }

    }

    /**
     * return the list
     *
     * @return the list
     */
    public synchronized LinkedList getList() {
        return list;
    }

    /**
     * return the size of the list
     *
     * @return size of the list
     */
    public synchronized int size() {
        return list.size();
    }

    /**
     * clear the whole list
     */
    public synchronized void clear() {
        list.clear();
    }

    /*
     * Add the object to the end of the list
     */
    public synchronized void push(Object object) {
        list.addLast(object);
    }

    /*
    * Remove and return the first item in the list
	*/
    public synchronized Object pop() {
        return list.removeFirst();
    }

    /*public String toOrderString(){
         String data = "";
         for (int i = 0; i < list.size(); i++)  {
             BidAskOrder order = (BidAskOrder)list.get(i);
             data += order.toString() + "\r\n";
         }
         return data;
     }

     public String toPriceString(){
         String data = "";
         for (int i = 0; i < list.size(); i++)  {
             BidAskPrice order = (BidAskPrice)list.get(i);
             data += order.toString() + "\r\n";
         }
         return data;
     } */
}




