package com.isi.csvr.shared;

import javax.swing.*;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Feb 25, 2005
 * Time: 2:52:04 PM
 */
public class TWComboModel extends DefaultComboBoxModel {
    private ArrayList exchangeCodes;

    public TWComboModel(ArrayList items) {
        this.exchangeCodes = items;
    }

    public Object getElementAt(int index) {
        return exchangeCodes.get(index);
    }

    public int getSize() {
        try {
            return exchangeCodes.size();
        } catch (Exception e) {
//            e.printStackTrace();
            return 0;
        }
    }
}
