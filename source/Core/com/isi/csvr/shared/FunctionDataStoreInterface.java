package com.isi.csvr.shared;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Oct 12, 2006
 * Time: 12:29:19 PM
 * To change this template use File | Settings | File Templates.
 */
public interface FunctionDataStoreInterface {

    FunctionStockInterface getStockObject(String key);
}
