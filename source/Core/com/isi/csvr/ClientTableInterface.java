package com.isi.csvr;

import com.isi.csvr.datastore.Symbols;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Nov 25, 2003
 * Time: 3:55:11 PM
 * To change this template use Options | File Templates.
 */
public interface ClientTableInterface {
    public Symbols getSymbols();

    public void cancelSorting();

    public boolean isCustomType();

    public void showInvalidDropMessage();
}
