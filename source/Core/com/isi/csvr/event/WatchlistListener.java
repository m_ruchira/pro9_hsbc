package com.isi.csvr.event;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 4, 2005
 * Time: 2:45:18 PM
 */
public interface WatchlistListener {

    public void symbolAdded(String key, String listID);

    public void symbolRemoved(String key, String listID);

    public void watchlistAdded(String listID);

    public void watchlistRenamed(String listID);

    public void watchlistRemoved(String listID);


}
