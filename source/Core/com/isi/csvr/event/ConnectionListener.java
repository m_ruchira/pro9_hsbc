package com.isi.csvr.event;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Aug 21, 2003
 * Time: 10:31:42 AM
 * To change this template use Options | File Templates.
 */
public interface ConnectionListener {

    /**
     * Connected to the server
     */
    public void twConnected();

    /**
     * Connection with the server failed
     */
    public void twDisconnected();
}
