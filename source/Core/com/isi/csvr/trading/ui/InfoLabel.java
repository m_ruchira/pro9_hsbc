package com.isi.csvr.trading.ui;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Sep 1, 2004
 * Time: 9:16:31 AM
 */
class InfoLabel extends JLabel {

    public InfoLabel(String text) {
        super(text);
        setForeground(Color.black);
        setHorizontalAlignment(SwingUtilities.LEADING);
        setFont(getFont().deriveFont(Font.BOLD));
    }

    public InfoLabel() {
        super();
        setForeground(Color.black);
        setHorizontalAlignment(SwingUtilities.LEADING);
        setFont(getFont().deriveFont(Font.BOLD));
    }

    public InfoLabel(int alignment) {
        super();
        setHorizontalAlignment(alignment);
        setForeground(Color.black);
        setFont(getFont().deriveFont(Font.BOLD));
    }
}
