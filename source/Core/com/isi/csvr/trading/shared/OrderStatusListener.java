package com.isi.csvr.trading.shared;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Oct 5, 2006
 * Time: 11:59:49 AM
 * To change this template use File | Settings | File Templates.
 */
public interface OrderStatusListener {

    void orderStatusChanged(String mubasherOrderNo);

//    void newOrderAdded(String mubasherOrderNo);
}
