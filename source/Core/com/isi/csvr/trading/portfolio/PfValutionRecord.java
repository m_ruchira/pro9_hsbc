package com.isi.csvr.trading.portfolio;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Dec 11, 2007
 * Time: 2:01:44 PM
 * To change this template use File | Settings | File Templates.
 */


public class PfValutionRecord {

    public static  int PORTFOLIODETAIL = 0;
    public static  int TOTOAL= 1;
    public static  int  VALRECORD = 2;
    public static  int EMPTYROW = 3;




    private int iRecType  = 0;
    ValuationRecord valRecord ;

    private String pfName  = "";
    private  String currency = "";
    private int prePfNo = 0;
    private String pfId = "";


    public PfValutionRecord(ValuationRecord valRec,int type){
        
        this.valRecord = valRec;
        this.iRecType = type;

    }


  public void  setValRecord(ValuationRecord valrec ){

      this.valRecord = valrec;

  }
  public ValuationRecord getValRecord(){
      return valRecord;
  }

    public int getRecType (){
        return iRecType;
    }

    public void setRecType (int valRec){
        this.iRecType = valRec;

    }
   public void setPfName (String pfName ){
        this.pfName = pfName;

    }
    public String getPfName (){
        return  pfName;

    }
    public void setCurrency(String currency){
        this.currency = currency;

    }
    public String getCurrency (){
        return  currency;

    }


    public void setPrePfNo(int prePfNo){
        this.prePfNo = prePfNo;
    }

    public int getPrePfNo(){
        return prePfNo;
    }

     public void setPfID(String  pfID){
        this.pfId = pfID;
    }

    public String getPfID(){
        return pfId;
    }


}
