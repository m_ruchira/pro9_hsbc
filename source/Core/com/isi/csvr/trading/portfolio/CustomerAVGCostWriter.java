package com.isi.csvr.trading.portfolio;

import com.isi.csvr.shared.Settings;

import java.io.IOException;
import java.io.FileWriter;
import java.util.TimeZone;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: 10-Apr-2007
 * Time: 16:13:29
 * To change this template use File | Settings | File Templates.
 */
public class CustomerAVGCostWriter {
     protected static String avgDetail = Settings.SYSTEM_PATH +"/avg.txt";

     public static void write(String s) throws IOException {
       
        FileWriter fWriter = new FileWriter(avgDetail, true);
        fWriter.write(s+ System.getProperty("line.separator"));
        fWriter.flush();
        fWriter.close();
    }
}
