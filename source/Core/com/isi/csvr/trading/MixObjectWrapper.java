package com.isi.csvr.trading;

import com.dfn.mtr.mix.beans.MIXObject;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 9, 2009
 * Time: 3:39:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class MixObjectWrapper extends MIXObject{

   private byte path;
   private String frame;

    public byte getPath() {
        return path;
    }

    public void setPath(byte path) {
        this.path = path;
    }

    public String getFrame() {
        return frame;
    }

    public void setFrame(String frame) {
        this.frame = frame;
    }
}
