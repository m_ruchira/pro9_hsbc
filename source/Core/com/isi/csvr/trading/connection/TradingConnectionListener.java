package com.isi.csvr.trading.connection;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Nov 3, 2004
 * Time: 2:09:50 PM
 * To change this template use File | Settings | File Templates.
 */
public interface TradingConnectionListener {

    public void tradeServerConnected();

    public void tradeSecondaryPathConnected();

    public void tradeServerDisconnected();
}
