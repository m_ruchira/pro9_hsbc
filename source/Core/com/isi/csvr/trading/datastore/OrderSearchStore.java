package com.isi.csvr.trading.datastore;

import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.trading.TradeMethods;

import java.io.Serializable;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: May 27, 2004
 * Time: 1:44:46 PM
 */
public class OrderSearchStore implements Serializable {
    private DynamicArray store;
    private DynamicArray todayStore;
//    private DynamicArray filteredStore;
    private boolean includeExecuted = false;
    private Hashtable pageStore;
    private int currentPage;
    public static OrderSearchStore self = null;
    private boolean canGoNext;
    private boolean canGoPrev;
    private String tableName;

    private OrderSearchStore() {
        try {
//            loadData();
            store = new DynamicArray();
            todayStore = new DynamicArray();
            pageStore = new Hashtable();

//            filteredStore = new DynamicArray();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static synchronized OrderSearchStore getSharedInstance() {
        if (self == null) {
            self = new OrderSearchStore();
        }
        return self;
    }

    public void addTransaction(Transaction transaction) {
        store.add(transaction);
        TradeMethods.getSharedInstance().getOrderSearchWindow().enableButtons();
//        applyFilter();

    }

    public void addTodayTransaction(Transaction transaction) {
        //      TodayTradeModel.getTotalAvgCostPrice();
        Transaction oldTransaction;
        OrderSearchStore orderSearchStore = OrderSearchStore.getSharedInstance();
        double avgCostPrice = 0;
        double netTrade = 0;
        double prvAvgCostPrice = 0;
        long prvRemaining = 0;
        int storeSize = orderSearchStore.todaySize();
        //    long oldQuantity = 0;
        /*for (int i = 0; i < OrderSearchStore.getSharedInstance().todaySize(); i++) {
            oldTransaction = OrderSearchStore.getSharedInstance().getTodayTransaction(i);
            if (oldTransaction.getSide() == TradeMeta.BUY) {
                avgCostPrice = calculateBuyAvgCost(oldTransaction.getPrice() * oldTransaction.getFilledQuantity(), oldTransaction.getCommission(), avgCostPrice, oldQuantity, oldTransaction.getFilledQuantity());
                oldQuantity = oldQuantity + oldTransaction.getFilledQuantity();
            } else if (oldTransaction.getSide() == TradeMeta.SELL) {
                avgCostPrice = calculateSellAvgCost(oldTransaction.getPrice() * oldTransaction.getFilledQuantity(), oldTransaction.getCommission(), avgCostPrice, oldQuantity, oldTransaction.getFilledQuantity());
                oldQuantity = oldQuantity - oldTransaction.getFilledQuantity();
            }
        }*/
        if (storeSize != 0) {
            for (int j = 1; j <= storeSize; j++) {
                Transaction tra = orderSearchStore.getTodayTransaction(storeSize - j);
                if (!tra.getSymbol().equalsIgnoreCase(transaction.getSymbol())) continue;
                prvAvgCostPrice = tra.getTodayAvgCostPrc();
                prvRemaining = tra.getTodayRemaning();
                break;
            }
        }

        long nowRemaining = 0;
        if (transaction.getSide() == TradeMeta.BUY) {
            avgCostPrice = calculateBuyAvgCost(transaction.getAvgPrice() * transaction.getFilledQuantity(), transaction.getCommission_for_Filled(), prvAvgCostPrice, prvRemaining, transaction.getFilledQuantity());
            netTrade = transaction.getNetAmount() + transaction.getCommission_for_Filled();
            nowRemaining = prvRemaining + transaction.getFilledQuantity();
        } else if (transaction.getSide() == TradeMeta.SELL) {
            avgCostPrice = calculateSellAvgCost(transaction.getAvgPrice() * transaction.getFilledQuantity(), transaction.getCommission_for_Filled(), prvAvgCostPrice, prvRemaining, transaction.getFilledQuantity());
            netTrade = transaction.getNetAmount() - transaction.getCommission_for_Filled();
            nowRemaining = prvRemaining - transaction.getFilledQuantity();
        }
        transaction.setTodayAvgCostPrc(avgCostPrice);
        //    transaction.setTodayTradeAmt(transaction.getFilledQuantity() * transaction.getAvgPrice());
        transaction.setTodayNetTrade(netTrade);
        transaction.setTodayRemaning(nowRemaining);
        todayStore.add(transaction);
        TradeMethods.getSharedInstance().getTodayTradeWindow().enableButtons();
//        applyFilter();

    }

    private double calculateBuyAvgCost(double totalOrderValue, double commision, double oldCostPrice, long oldQty, long qty) {
        if ((oldQty + qty) != 0) {
            return ((oldQty * oldCostPrice) + (totalOrderValue + commision)) / (oldQty + qty);
        }
        return 0.0;
    }

    private double calculateSellAvgCost(double totalOrderValue, double commision, double oldCostPrice, long oldQty, long qty) {
        if ((oldQty - qty) != 0) {
            return ((oldQty * oldCostPrice) - (totalOrderValue - commision)) / (oldQty - qty);
        }
        return 0.0;
    }

    public Transaction getTransaction(int index) {
        return (Transaction) store.get(index);
    }

    public Transaction getTodayTransaction(int index) {
        if (todayStore != null)
            return (Transaction) todayStore.get(index);
        else
            return null;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public Transaction searhTransactionByOrderID(String id) {
        Transaction transaction;
        for (int i = 0; i < store.size(); i++) {
            transaction = (Transaction) store.get(i);
            if ((transaction.getMubasherOrderNumber() != null) && (transaction.getMubasherOrderNumber().equals(id)))
                return transaction;
            transaction = null;
        }
        return null;
    }

    public Transaction searhTransactionByClOrderID(String id) {
        Transaction transaction;
        for (int i = 0; i < store.size(); i++) {
            transaction = (Transaction) store.get(i);
            if ((transaction.getClOrderID() != null) && (transaction.getClOrderID().equals(id)))
                return transaction;
            transaction = null;
        }
        return null;
    }

    public Transaction searhTransactionByMubasherOrderID(String id) {
        Transaction transaction;
        for (int i = 0; i < store.size(); i++) {
            transaction = (Transaction) store.get(i);
            if ((transaction.getMubasherOrderNumber() != null) && (transaction.getMubasherOrderNumber().equals(id)))
                return transaction;
            transaction = null;
        }
        return null;
    }

    public Transaction searhTodayTransactionByClOrderID(String id) {
        Transaction transaction;
        for (int i = 0; i < todayStore.size(); i++) {
            transaction = (Transaction) todayStore.get(i);
            if ((transaction.getClOrderID() != null) && (transaction.getClOrderID().equals(id)))
                return transaction;
            transaction = null;
        }
        return null;
    }

    public Transaction searhTodayTransactionByMubasherOrderID(String id) {
        Transaction transaction;
        for (int i = 0; i < todayStore.size(); i++) {
            transaction = (Transaction) todayStore.get(i);
            if ((transaction.getMubasherOrderNumber() != null) && (transaction.getMubasherOrderNumber().equals(id)))
                return transaction;
            transaction = null;
        }
        return null;
    }

    public void removeTransactionByOrderID(long id) {
        Transaction transaction;
        for (int i = 0; i < store.size(); i++) {
            transaction = (Transaction) store.get(i);
            if ((transaction.getOrderID() != null) && (transaction.getOrderID().equals(id))) {
                store.remove(i);
            }
            transaction = null;
        }
//        applyFilter();
    }

    public void replaceTransactionByOrderID(Transaction newTransaction) {
        Transaction transaction;
        for (int i = 0; i < store.size(); i++) {
            transaction = (Transaction) store.get(i);
            if ((transaction.getOrderID() != null) && (transaction.getOrderID().equals(newTransaction.getOrderID()))) {
                //if (newTransaction.getSequenceNumber() > transaction.getSequenceNumber()){
                store.set(i, newTransaction);
                break;
                //}
            }
            transaction = null;
        }
    }

    public Transaction searhTransactionByMessageID(long id) {
        Transaction transaction;
        for (int i = 0; i < store.size(); i++) {
            transaction = (Transaction) store.get(i);
            if (transaction.getMessageID() == id)
                return transaction;
            transaction = null;
        }
        return null;
    }

    public void removeTransactionByMessageID(long id) {
        Transaction transaction;
        for (int i = 0; i < store.size(); i++) {
            transaction = (Transaction) store.get(i);
            if (transaction.getMessageID() == id)
                store.remove(i);
            transaction = null;
        }
//        applyFilter();
    }

    public void replaceTransactionByMessageID(Transaction newTransaction) {
        Transaction transaction;
        for (int i = 0; i < store.size(); i++) {
            transaction = (Transaction) store.get(i);
            if (transaction.getMessageID() == newTransaction.getMessageID()) {
                store.set(i, newTransaction);
                break;
            }
            transaction = null;
        }
    }

    public boolean isIncludeExecuted() {
        return includeExecuted;
    }

//    public void setIncludeExecuted(boolean includeExecuted) {
//        this.includeExecuted = includeExecuted;
////        applyFilter();
//    }

//    public void applyFilter(){
//        Transaction transaction;
//        filteredStore.clear();
//        for (int i = 0; i < store.size(); i++) {
//            transaction = (Transaction)store.get(i);
//            if ((transaction.getStatus() == TradeMeta.T39_Filled)){
//                if (isIncludeExecuted()){
//                    filteredStore.add(transaction);
//                }
//            }else{
//                filteredStore.add(transaction);
//            }
//            transaction = null;
//        }
//    }

    public void setNewPage() {
        currentPage++;
        if (currentPage <= 1) {
            setCanGoPrev(false);
        } else {
            setCanGoPrev(true);
        }
    }

    public void goPageBack() {
        currentPage--;
        currentPage--;
        if (currentPage <= 1) {
            setCanGoPrev(false);
        } else {
            setCanGoPrev(true);
        }
    }

    public long getPageTop() {
        PageRecord pageRecord = (PageRecord) pageStore.get("" + currentPage);
        if (pageRecord == null) {
            return Integer.MAX_VALUE;
        } else {
            return pageRecord.top;
        }
    }

    public long getPageBottom() {
        PageRecord pageRecord = (PageRecord) pageStore.get("" + currentPage);
        if (pageRecord == null) {
            return Long.MAX_VALUE;
        } else {
            return pageRecord.bottom;
        }
    }

    public void setCurrentPageTop(long id) {
        try {
            PageRecord pageRecord = (PageRecord) pageStore.get("" + currentPage);
            if (pageRecord == null) {
                pageRecord = new PageRecord();
                pageStore.put("" + currentPage, pageRecord);
            }
            if (pageRecord.top == -1)
                pageRecord.top = id;
        } catch (Exception ex) {

        }
    }

    public void setCurrentPageBottom(long id) {
        PageRecord pageRecord = (PageRecord) pageStore.get("" + currentPage);
        if (pageRecord == null) {
            pageRecord = new PageRecord();
            pageStore.put("" + currentPage, pageRecord);
        }
        if (pageRecord.bottom == -1)
            pageRecord.bottom = id;
    }

    public int size() {
        return store.size();
    }

    public int todaySize() {
        return todayStore.size();
    }

    public void clear() {
        store.clear();
        store.trimToSize();
    }

    public void clearTodayTrades() {
        todayStore.clear();
        todayStore.trimToSize();
    }

    public void init() {
        store.clear();
        store.trimToSize();
        todayStore.clear();
        todayStore.trimToSize();
        pageStore.clear();
        currentPage = 0;
        tableName = "*";
    }

    public boolean canGoNext() {
        return canGoNext;
    }

    public void setCanGoNext(boolean canGoNext) {
        this.canGoNext = canGoNext;
    }

    public boolean canGoPrev() {
        return canGoPrev;
    }

    public void setCanGoPrev(boolean canGoPrev) {
        this.canGoPrev = canGoPrev;
    }

    private class PageRecord {
        public long top = -1;
        public long bottom = -1;
    }
}
