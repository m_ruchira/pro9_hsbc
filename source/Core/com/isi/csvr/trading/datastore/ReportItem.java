package com.isi.csvr.trading.datastore;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Nov 22, 2005
 * Time: 2:00:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class ReportItem {
    private String description;
    private String url;

    public ReportItem(String description, String url) {
        this.description = description;
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl() {
        return url;
    }
}
