package com.isi.csvr.dde;

import com.isi.csvr.table.Table;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 23, 2004
 * Time: 1:59:34 PM
 * To change this template use File | Settings | File Templates.
 */
public interface DDELinkInterface {
    public void getDDEString(Table table, boolean withHeadings);
}
