package com.isi.csvr;

import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

public class NullPrintStream extends PrintStream {

    public NullPrintStream(OutputStream out) {
        super(out);
    }

    public NullPrintStream(OutputStream out, boolean autoFlush) {
        super(out, autoFlush);
    }

    public NullPrintStream(OutputStream out, boolean autoFlush, String encoding)
            throws UnsupportedEncodingException {
        super(out, autoFlush, encoding);
    }


    public void print(boolean b) {
    }

    public void print(char b) {
    }

    public void print(char[] b) {
    }

    public void print(double b) {
    }

    public void print(float b) {
    }

    public void print(int b) {
    }

    public void print(long b) {
    }

    public void print(Object b) {
    }

    public void print(String b) {
    }

    public void println(boolean b) {
    }

    public void println(char b) {
    }

    public void println(char[] b) {
    }

    public void println(double b) {
    }

    public void println(float b) {
    }

    public void println(int b) {
    }

    public void println(long b) {
    }

    public void println(Object b) {
    }

    public void println(String b) {
    }

    public void write(byte[] buf, int off, int len) {
    }

    public void write(int b) {
    }
}