// Copyright (c) 2000 ISI
package com.isi.csvr.datastore;

import com.isi.csvr.shared.SharedMethods;

import java.io.Serializable;

/**
 * Holds data associated with asset classes
 * <p/>
 *
 * @author - Nishantha Fernando.
 *         Modified by  - Uditha Nagahawatta.
 */
public class AssetClass implements Serializable {

    private String exchangeCode;
    private String assetSymbol;
    private String caption = "";

    private long marketVolume;
    private double marketTournover;
    private int numberOfTrades;
    private short numberOfUps;
    private short numberOfDown;
    private short numberOfNoChange;
    private short numberOfSymbolsTraded;
    private byte marketStatus = -1;


    /**
     * Constructor
     */
    public AssetClass(String assetSymbol) {
        /*  there must be a class which has all the previous close proces for assetes & the symbols
from the database. this will use to get the balances in case which is not availabe in the feed */
        this.assetSymbol = assetSymbol;
    } // end construction

    public void setData(String[] data) throws Exception {
        if (data == null)
            return;

        char tag;
        for (int i = 0; i < data.length; i++) {
            if (data[i] == null)
                continue;
            if (data[i].length() <= 1)
                continue;
            tag = data[i].charAt(0);
            switch (tag) {
                case 'A':
                    assetSymbol = data[i].substring(1);
                    break;
                case 'B':
                    marketTournover = SharedMethods.getDouble(data[i].substring(1));
                    break;
                case 'C':
                    marketVolume = SharedMethods.getLong(data[i].substring(1));
                    break;
                case 'D':
                    numberOfUps = (short) SharedMethods.getLong(data[i].substring(1));
                    break;
                case 'E':
                    numberOfDown = (short) SharedMethods.getLong(data[i].substring(1));
                    break;
                case 'F':
                    numberOfNoChange = (short) SharedMethods.getLong(data[i].substring(1));
                    break;
                case 'G':
                    numberOfSymbolsTraded = (short) SharedMethods.getLong(data[i].substring(1));
                    break;
                case 'H':
                    numberOfTrades = (int) SharedMethods.getLong(data[i].substring(1));
                    break;
                case 'I':
                    marketStatus = (byte) SharedMethods.getLong(data[i].substring(1));
                    break;
            }

        }

    }


    public String getAssetSymbol() {
        return assetSymbol;
    }

    public void setAssetSymbol(String newAssetSymbol) {
        assetSymbol = newAssetSymbol;
    }

    public String getExchangeCode() {
        return exchangeCode;
    }

    public void setExchangeCode(String newExchangeCode) {
        exchangeCode = newExchangeCode;
    }

    public double getMarketTournover() {
        return marketTournover;
    }

    public void setMarketTournover(double newMarketTournover) {
        marketTournover = newMarketTournover;
    }

    public long getMarketVolume() {
        return marketVolume;
    }

    public void setMarketVolume(long newMarketVolume) {
        marketVolume = newMarketVolume;
    }

    public short getNumberOfDown() {
        return numberOfDown;
    }

    public void setNumberOfDown(short newNumberOfDown) {
        numberOfDown = newNumberOfDown;
    }

    public short getNumberOfNoChange() {
        return numberOfNoChange;
    }

    public void setNumberOfNoChange(short newNumberOfNoChange) {
        numberOfNoChange = newNumberOfNoChange;
    }

    public short getNumberOfSymbolsTraded() {
        return numberOfSymbolsTraded;
    }

    public void setNumberOfSymbolsTraded(short newNumberOfSymbolsTraded) {
        numberOfSymbolsTraded = newNumberOfSymbolsTraded;
    }

    public int getNumberOfTrades() {
        return numberOfTrades;
    }

    public void setNumberOfTrades(int newNumberOfTrades) {
        numberOfTrades = newNumberOfTrades;
    }

    public short getNumberOfUps() {
        return numberOfUps;
    }

    public void setNumberOfUps(short newNumberOfUps) {
        numberOfUps = newNumberOfUps;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String newCaption) {
        caption = newCaption;
    }

    public byte getMarketStatus() {
        return marketStatus;
    }

    public void setMarketStatus(byte newMarketStatus) {
        marketStatus = newMarketStatus;
    }


}



