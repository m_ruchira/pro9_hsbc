package com.isi.csvr.datastore;

// Copyright (c) 2000 Home

import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.event.ConnectionListener;
import com.isi.csvr.event.ConnectionNotifier;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.ClientTable;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Collection;


/**
 * Maintain a registry of Symbols objects.
 */
public class SymbolsRegistry {

    private static Hashtable<Symbols, Symbols> registry = null;
    private static Hashtable<Symbols, Symbols> customRegistry = null;
    private static Hashtable<String, Object> intermediateRegistry = null;

    private static SymbolsRegistry self = null;

    public static synchronized SymbolsRegistry getSharedInstance() {
        if (self == null) {
            self = new SymbolsRegistry();
        }
        return self;
    }

    public SymbolsRegistry() {
        intermediateRegistry = new Hashtable<String, Object>();
    }

    /**
     * register the given symbols object
     */
    public void register(Symbols symbols, boolean custom) {
        if (custom) {
            if (customRegistry == null) { // create the registry
                customRegistry = new Hashtable<Symbols, Symbols>(10, .9F);
            }
            customRegistry.put(symbols, symbols);
        } else {
            if (registry == null) { // create the registry
                registry = new Hashtable<Symbols, Symbols>(10, .9F);
            }
            registry.put(symbols, symbols);
        }
    }

    public void checkSymbol(String symbol) {


    }

    public boolean isSymbolIn(String key) {
        Enumeration<Symbols> symbols = customRegistry.elements();
        while (symbols.hasMoreElements()) {
            Symbols obj = symbols.nextElement();
            if (obj.isAlreadyIn(key)) {
                return true;
            }
            obj = null;
        }
        symbols = null;

        try {
            symbols = registry.elements();
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        if (symbols!=null) {
            while (symbols.hasMoreElements()) {
                Symbols obj = symbols.nextElement();
                if (obj.isAlreadyIn(key)) {
                    return true;
                }
                obj = null;
            }
        }

        return false;
    }

    public void removeSymbol(String symbol) {
        Collection<Symbols> elements = customRegistry.values();
        for (Symbols symbols : elements) {
            if (symbols.isAlreadyIn(symbol)) {
                symbols.removeSymbol(symbol);
            }
        }
        elements = null;
    }


    public void unregister(Symbols symbols, boolean custom) {
        if (custom) {
            Symbols registrySymbols = customRegistry.remove(symbols);
            registrySymbols = null;
        } else {
            Symbols registrySymbols = registry.remove(symbols);
            registrySymbols = null;
        }
    }

    public void rememberRequest(String id, Object data) {
        intermediateRegistry.put(id, data);
    }

    public Object removeRememberedRequest(String id) {
        try {
            System.out.println("Came to remove remember");
            return intermediateRegistry.remove(id);
        } catch (Exception e) {
            System.out.println("REMOVE REMEMBER Fails");
            e.printStackTrace();
            return null;
        }
    }
}