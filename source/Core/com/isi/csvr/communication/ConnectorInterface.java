package com.isi.csvr.communication;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Dec 31, 2004
 * Time: 9:02:33 AM
 * To change this template use File | Settings | File Templates.
 */
public interface ConnectorInterface {
    void closeSocket();

    int getMode();

    void setMode(int status);

    void setIdle();

    void start();

    void update();

//    void resetConnectionTime();
}
