package com.isi.csvr.history;

// Copyright (c) 2000 Home


public interface ExportListener {
    public void exportCompleted(Exception e);

    public void exportAborted(Exception e);

    public void exportProgressChanged(int iProgress, int iMax);
} 