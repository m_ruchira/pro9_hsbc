package com.isi.csvr.util;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author Uditha Nagahawatta
 * @version 1.0
 */


import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;

import javax.swing.*;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.plaf.basic.ComboPopup;
import javax.swing.plaf.metal.MetalComboBoxButton;
import javax.swing.plaf.metal.MetalComboBoxIcon;
import javax.swing.plaf.metal.MetalComboBoxUI;
import javax.swing.plaf.metal.MetalLookAndFeel;
import java.awt.*;

public class ExtendedComboUI extends MetalComboBoxUI {
    private Color arrowColor;


    protected JButton createArrowButton() {

        boolean iconOnly = (comboBox.isEditable());
        JButton button = new MetalComboBoxButton(comboBox,
                new CustomMetalComboBoxIcon(),
                iconOnly,
                currentValuePane,
                listBox);
        button.setMargin(new Insets(0, 1, 1, 3));
//        if (MetalLookAndFeel.usingOcean()) {
//            // Disabled rollover effect.
//            button.putClientProperty(MetalBorders.NO_BUTTON_ROLLOVER,
//                                     Boolean.TRUE);
//        }
//        updateButtonForOcean(button);
        button.setContentAreaFilled(false);
        return button;
    }

    public void setArrowButtonColor(Color clr) {
        arrowColor = clr;
    }

    protected ComboPopup createPopup() {

        BasicComboPopup popup = new BasicComboPopup(comboBox) {

            public void show() {
                GUISettings.applyOrientation(scroller);
                Dimension popupSize = ((ExtendedComboInterface) comboBox).getPopupSize();
                popupSize.setSize(popupSize.width,
                        getPopupHeightForRowCount(comboBox.getMaximumRowCount()));
                Rectangle popupBounds = computePopupBounds(0,
                        comboBox.getBounds().height,
                        popupSize.width,
                        popupSize.height);

                scroller.setMaximumSize(popupBounds.getSize());
                scroller.setPreferredSize(popupBounds.getSize());
                scroller.setMinimumSize(popupBounds.getSize());
                list.invalidate();
                int selectedIndex = comboBox.getSelectedIndex();
                if (selectedIndex == -1) {
                    list.clearSelection();
                } else {
                    list.setSelectedIndex(selectedIndex);
                }
                list.ensureIndexIsVisible(list.getSelectedIndex());
                setLightWeightPopupEnabled(comboBox.isLightWeightPopupEnabled());
                showPopup(this, popupBounds, 0, comboBox.getHeight());

            }
        };
        popup.getAccessibleContext().setAccessibleParent(comboBox);

        return popup;
    }

    public void showPopup(BasicComboPopup popup, Rectangle popupBounds, int x, int y) {
        if (!Language.isLTR()) {
            x = x - (int) popupBounds.getWidth() + (int) comboBox.getWidth();
        }
        Point point = new Point(x, y);
        Toolkit oToolkit = Toolkit.getDefaultToolkit();
        Dimension screenSize = oToolkit.getScreenSize();

        SwingUtilities.convertPointToScreen(point, comboBox);
        if (Language.isLTR()) {
            if ((point.getX() + popupBounds.getWidth()) > screenSize.getWidth()) {
                x = (int) (x - popupBounds.getWidth() + comboBox.getWidth());
            }
        } else {
            if (point.getX() < 0) {
                x = 0;
            }
        }

        if ((point.getY() + popupBounds.getHeight()) > screenSize.getHeight()) {
            y = (int) (y - popupBounds.getHeight() - comboBox.getHeight());
        }
        GUISettings.applyOrientation(popup);
        popup.show(comboBox, x, y);
    }

    public JPopupMenu getPopup() {
        //scroll
        return (JPopupMenu) popup;
    }

    public Color getArrowColor() {

        if (arrowColor != null) {
            return arrowColor;
        } else {
            return MetalLookAndFeel.getControlInfo();
        }

    }

    public class CustomMetalComboBoxIcon extends MetalComboBoxIcon {

        public void paintIcon(Component c, Graphics g, int x, int y) {
            JComponent component = (JComponent) c;
            int iconWidth = getIconWidth();

            g.translate(x, y);
            Color enabled;
            Color disabled = MetalLookAndFeel.getControlShadow();

            if (arrowColor != null) {
                enabled = arrowColor;
            } else {
                enabled = comboBox.getForeground();
            }


            g.setColor(component.isEnabled() ? enabled : disabled);
            g.drawLine(0, 0, iconWidth - 1, 0);
            g.drawLine(1, 1, 1 + (iconWidth - 3), 1);
            g.drawLine(2, 2, 2 + (iconWidth - 5), 2);
            g.drawLine(3, 3, 3 + (iconWidth - 7), 3);
            g.drawLine(4, 4, 4 + (iconWidth - 9), 4);

            g.translate(-x, -y);
        }


    }
}


