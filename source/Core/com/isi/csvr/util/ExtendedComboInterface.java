package com.isi.csvr.util;

import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Nov 18, 2003
 * Time: 3:28:52 PM
 * To change this template use Options | File Templates.
 */
public interface ExtendedComboInterface {
    public Dimension getPopupSize();
}
