package com.isi.csvr;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 26, 2005
 * Time: 10:07:33 PM
 */

import javax.swing.*;
import javax.swing.plaf.UIResource;
import javax.swing.plaf.metal.MetalLookAndFeel;
import java.awt.*;
import java.io.Serializable;

public class CheckBoxTest {
    public static void main(String args[]) {
        JFrame frame = new JFrame("CheckBoxTest");
        JCheckBox checkBox = new JCheckBox(new CheckBoxIcon());
        frame.setSize(150, 150);
        frame.getContentPane().add(checkBox);
        frame.setVisible(true);
    }
}

class CheckBoxIcon implements Icon, UIResource, Serializable {
    protected int getControlSize() {
        return 13;
    }

    public void paintIcon(Component c, Graphics g, int x, int y) {
        JCheckBox cb = (JCheckBox) c;
        ButtonModel model = cb.getModel();
        int controlSize = getControlSize();

        //boolean drawCheck = model.isSelected();

        if (model.isEnabled()) {
            if (model.isPressed() && model.isArmed()) {
                g.setColor(MetalLookAndFeel.getControlShadow());
                g.fillRect(x, y, controlSize - 1, controlSize - 1);
                drawPressed3DBorder(g, x, y, controlSize, controlSize);
            } else {
                drawFlush3DBorder(g, x, y, controlSize, controlSize);
            }
            g.setColor(MetalLookAndFeel.getControlInfo());
        } else {
            g.setColor(MetalLookAndFeel.getControlShadow());
            g.drawRect(x, y, controlSize - 1, controlSize - 1);
        }

        if (model.isSelected()) {
            drawCheck(c, g, x, y);
        }
    }

    protected void drawCheck(Component c, Graphics g, int x, int y) {
        int controlSize = getControlSize();
        g.drawLine(x + (controlSize - 4), y + 2, x + 3, y + (controlSize - 5));
        g.drawLine(x + (controlSize - 4), y + 3, x + 3, y + (controlSize - 4));
        g.drawLine(x + 3, y + 2, x + (controlSize - 4), y + (controlSize - 5));
        g.drawLine(x + 3, y + 3, x + (controlSize - 4), y + (controlSize - 4));
    }

    private void drawFlush3DBorder(Graphics g, int x, int y, int w, int h) {
        g.translate(x, y);
        g.setColor(MetalLookAndFeel.getControlDarkShadow());
        g.drawRect(0, 0, w - 2, h - 2);
        g.setColor(MetalLookAndFeel.getControlHighlight());
        g.drawRect(1, 1, w - 2, h - 2);
        g.setColor(MetalLookAndFeel.getControl());
        g.drawLine(0, h - 1, 1, h - 2);
        g.drawLine(w - 1, 0, w - 2, 1);
        g.translate(-x, -y);
    }

    private void drawPressed3DBorder(Graphics g, int x, int y, int w, int h) {
        g.translate(x, y);

        drawFlush3DBorder(g, 0, 0, w, h);

        g.setColor(MetalLookAndFeel.getControlShadow());
        g.drawLine(1, 1, 1, h - 2);
        g.drawLine(1, 1, w - 2, 1);
        g.translate(-x, -y);
    }

    public int getIconWidth() {
        return getControlSize();
    }

    public int getIconHeight() {
        return getControlSize();
    }
}
