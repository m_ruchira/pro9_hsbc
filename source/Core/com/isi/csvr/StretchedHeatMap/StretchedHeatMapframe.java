package com.isi.csvr.StretchedHeatMap;

import com.isi.csvr.heatmap.HeatRecord;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.TWTextArea;
import com.isi.csvr.table.SmartFileChooser;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;
import com.isi.csvr.Client;
import com.isi.csvr.*;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.util.ListButtonItem;

import javax.swing.*;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;
import javax.swing.event.InternalFrameEvent;
import javax.swing.text.*;
import javax.swing.text.html.MinimalHTMLWriter;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: Mar 1, 2007
 * Time: 1:44:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class StretchedHeatMapframe extends InternalFrame implements Themeable, ActionListener, KeyListener, MouseListener {

    public static Hashtable heatHash = new Hashtable();
    JPanel topPanel;
    JScrollPane heatMapScroll = new JScrollPane();
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

    public static DynamicArray heatStore = new DynamicArray();
    public static DynamicArray heatBuffer = new DynamicArray();
    final Object lock = new Object();
    GridPanel gridpanel = new GridPanel();

    private JPopupMenu stretchedPanelPopup;
    private TWMenu criteria;
    private TWMenu exchange;
    private static StretchedHeatMapframe self;
    String[] symbolArray = null;
    public boolean updating_enable=false;
   // public static JLabel max_lbl=new JLabel();
   // public static JLabel min_lbl=new JLabel();

    public static StretchedHeatMapframe getSharedInstance(String[] hSymbols, String Title) {
        if (self == null) {
            self = new StretchedHeatMapframe(hSymbols, Title);
        } else {
            self.updating_enable=false;
            self.dispose();
            self = new StretchedHeatMapframe(hSymbols, Title);
        }
        return self;
    }


    public StretchedHeatMapframe(String[] hSymbols, String Title) {
        super();
        //updateHeatStore(hSymbols);
        symbolArray = hSymbols;
        gridpanel.updateHeatStore(hSymbols);
        gridpanel.setPreferredSize(new Dimension(gridpanel.setPanelWidth(), 93));
//        min_lbl.setOpaque(true);
//        max_lbl.setOpaque(true);
//        max_lbl.setBackground(Color.green);
//         min_lbl.setBackground(Color.red);
        createUI();
        this.setSize(1020, 110);
        this.setLocation(0, 0);
        this.setTitle(Language.getString("HEATMAP_TITLE")+" - "+Title);
        this.pack();
        this.setLayer(GUISettings.TOP_LAYER);
        setMaximumSize(new Dimension(2000,getHeight()));
        Theme.registerComponent(this);
        GUISettings.applyOrientation(this);
        applyTheme();
        //this.setVisible(true);
        updating_enable=true;
        valueUpdator updator = new  valueUpdator();
        updator.start();

    }

//    public void updatePanel(String [] hSymbols){
//       gridpanel.updateHeatStore(hSymbols);
//       gridpanel.setPreferredSize(new Dimension(gridpanel.setPanelWidth(),93));
//    }

    public void createUI() {
        int width=0;
        int height=0;
        if(gridpanel.setPanelWidth()<=1020){
              width=gridpanel.setPanelWidth()+3;
              height=68;
        }else{
            width=1020;
            height=83;
        }
        String[] namePanelWidths = {""+width};
        String[] namePanelHeights = {""+height};
        //FlexGridLayout flexGridLayout1 = new FlexGridLayout(namePanelWidths, namePanelHeights);
        topPanel = new JPanel();
        topPanel.setSize(gridpanel.setPanelWidth(),83);
        topPanel.setLayout(new BorderLayout());
        getContentPane().add(topPanel, BorderLayout.CENTER);

        heatMapScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
        heatMapScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        heatMapScroll.setSize(gridpanel.setPanelWidth(), 83);
        heatMapScroll.getViewport().add(gridpanel);
         //topPanel.add(max_lbl);
        topPanel.add(heatMapScroll);
         //topPanel.add(min_lbl);

        gridpanel.addMouseListener(this);
    }

    public void loadSymbolHash(String[] sSymbols) {
//        //Hashtable heatHash=new Hashtable();
//        for(int i=0;i<=sSymbols.length-1;i++){
//        String splitPattern = "~";
//        String[] symbol = null;
//        symbol =sSymbols[i].split(splitPattern);
//        heatHash.put(i,new StretchedHeatRecord(symbol[1],0));
//
//        }
//        System.out.println("Loading completed......");
    }

    public void updateHeatStore(String[] sa) {
        heatBuffer.clear();
        if (sa != null) {
            // HeatInterface.updateHeatStore(sa, heatBuffer);
        }
//        synchronized (lock) {
//            DynamicArray tmpDA = heatStore;
//            heatStore = heatBuffer;
//            heatBuffer = tmpDA;
//
//        }
       // System.out.println("buffer size  : " + heatBuffer.size());

    }


    public void keyTyped(KeyEvent e) {

    }

    public void keyPressed(KeyEvent e) {

    }

    public void keyReleased(KeyEvent e) {

    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this);

    }

    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() > 1) {
            String s = gridpanel.getSelectedSymbol();
            if (s != null) {
                HeatInterface.showSummaryQuote(s);
                //Client.getInstance().mnu_SummaryQuoteSymbol(s, null,null, false, false);
            }
        }
        if (SwingUtilities.isRightMouseButton(e)) {
            createPopupMenu();
            //validatePopupMenu();
            stretchedPanelPopup.show(e.getComponent(), e.getX(), e.getY());

        }
    }

    /**
     * Invoked when a mouse button has been pressed on a component.
     */
    public void mousePressed(MouseEvent e) {
        Object obj = e.getSource();
        if (obj == gridpanel) {
            System.out.println("Testing Mouse Action");
            gridpanel.updateSelectedIndex(e.getX(), e.getY());
            gridpanel.repaint();
            // footer.repaint();
            // hintPane.setVisible(false);
        }
    }

    /**
     * Invoked when a mouse button has been released on a component.
     */
    public void mouseReleased(MouseEvent e) {
    }

    /**
     * Invoked when the mouse enters a component.
     */
    public void mouseEntered(MouseEvent e) {
    }

    /**
     * Invoked when the mouse exits a component.
     */
    public void mouseExited(MouseEvent e) {
        // hintPane.setVisible(false);
    }

    public void createPopupMenu() {
        stretchedPanelPopup = new JPopupMenu();
        criteria = new TWMenu("Criteria");
        exchange = new TWMenu("Exchange");
        stretchedPanelPopup.add(criteria);
        stretchedPanelPopup.add(exchange);
        creatCriteriaList();
        createExchangeList();

    }

    public void creatCriteriaList() {
        try {
            Object[] items = new Object[HeatInterface.getCriteriaDescriptions().length];
            for (int i = 0; i < items.length; i++) {
                TWMenuItem criteria_menu = new TWMenuItem(HeatInterface.getCriteriaDescriptions()[i]);
                criteria.add(criteria_menu);
                //items[i] = lbi;
            }
        } catch (Exception e) {

        }

    }

    public void createExchangeList() {
        //   ArrayList items = new ArrayList();
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        //TWMenuItem select_menu  = new TWMenuItem(Language.getString("SELECT_EXCHANGE"));
        //exchange.add(select_menu);
        while (exchanges.hasMoreElements()) {
            Exchange exchangee = (Exchange) exchanges.nextElement();
            if (exchangee.isDefault()) {
                TWMenuItem exchange_menu = new TWMenuItem(exchangee.getDescription());
                exchange.add(exchange_menu);
            }
            exchangee = null;
        }

    }

    public class valueUpdator extends Thread {

        public void run() {

            while (updating_enable) {
                try {
                    sleep(1000);
                    gridpanel.updateHeatStore(symbolArray);
                    gridpanel.setPreferredSize(new Dimension(gridpanel.setPanelWidth(), 93));
                    gridpanel.repaint();
                   // System.out.println("Running updator");
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

}
