package com.isi.csvr.ticker.custom;

import com.isi.csvr.ticker.SharedSettings;

import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class TickerFrameListener implements InternalFrameListener {

    public TickerFrameListener() {
    }

    public void internalFrameOpened(InternalFrameEvent e) {

    }

    public void internalFrameClosing(InternalFrameEvent e) {
        //System.err.println("ticker frame closed");
        TradeFeeder.setVisible(false);
        SharedSettings.selectCustomTicker(false);
        SharedSettings.custom = SharedSettings.deselect;
    }

    public void internalFrameClosed(InternalFrameEvent e) {

    }

    public void internalFrameIconified(InternalFrameEvent e) {
    }

    public void internalFrameDeiconified(InternalFrameEvent e) {
    }

    public void internalFrameActivated(InternalFrameEvent e) {
        TradeFeeder.setVisible(true);
    }

    public void internalFrameDeactivated(InternalFrameEvent e) {
    }
}