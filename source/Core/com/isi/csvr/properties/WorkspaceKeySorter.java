package com.isi.csvr.properties;

import java.util.Comparator;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class WorkspaceKeySorter implements Comparator {
    int value = 0;

    public int compare(Object o1, Object o2) {
        String key1 = (String) o1;
        String key2 = (String) o2;

        return (getKey(key1) - getKey(key2));
    }

    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    private int getKey(String sTag) {
        try {
            sTag = sTag.trim();

            if (sTag.length() > 0) {

                int iPipe = sTag.indexOf("|");

                if (iPipe == -1) {
                    value = Integer.parseInt(sTag);
                } else {
                    value = Integer.parseInt(sTag.substring(0, iPipe));
                }


                if (value == 1045) { // make filtered TnS the largest value
                    return 9999;
                } else {
                    return value;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return 0;
    }
}