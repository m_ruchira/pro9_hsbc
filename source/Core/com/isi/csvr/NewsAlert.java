// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr;

/**
 * Displays given messages in a popup window.
 * <P>
 * @author Uditha Nagahawatta
 */

import java.util.Vector;

public class NewsAlert {
    String g_sType;
    Vector g_oMessageList;

    /**
     * Constructor
     */
    public NewsAlert(String sType) {
        g_sType = sType;
        g_oMessageList = new Vector();
    }

    //public void addMessage(String sID, String sMessage,){
    //}

    /**
     */
    public void addMessage(String sID, String sMessage, String sValue) {
        try {
            /* Check if the message is already receved */
            if (g_oMessageList.contains(sID)) {
                return;
            } else {
                /* Show the message */
                showMessage(sMessage, g_sType, sValue);
                /* Add the message id to the list so that it will
                    not be shown again in the future*/
                g_oMessageList.add(sID);
            }
        } catch (Exception e) {
            e.printStackTrace();
            // malformed message
        }


    }

    /**
     * Displays the messgae
     */
    private void showMessage(String sMessage, String sType, String sValue) {

        //(new ShowMessage(UnicodeToNative.getNativeString(sMessage),sType));
        ShowMessage sMsg = new ShowMessage();
        if (sType.equals("A"))
            sMsg.showAletPopup(sMessage, sValue);
        else
            sMsg.showNewsPopup(sMessage, sValue);
    }
}
