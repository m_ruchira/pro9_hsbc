// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr;

/**
 * Contains custom exceptions used in the application
 * <p/>
 *
 * @author Uditha Nagahawatta
 */
public class Exceptions extends Exception {
    Exceptions() {
        super();
    }
}

/**
 * Thrown when the user name is invalud
 */
class InvalidUserException extends Exception {
    InvalidUserException() {
        super();
    }
}

/**
 * Thrown when the password is invalud
 */
class InvalidPasswordException extends Exception {
    InvalidPasswordException() {
        super();
    }
}

/**
 * Thrown when the authentication is invalud
 */
class InvalidAuthenticationException extends Exception {
    InvalidAuthenticationException() {
        super();
    }
}

