package com.isi.csvr.symbolselector;

import com.isi.csvr.shared.GUISettings;

import javax.swing.*;
import java.awt.*;

//import com.isi.csvr.Themeable;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class CompanySelectorRenderer extends JLabel
        implements ListCellRenderer {

    private Company company = null;

    public CompanySelectorRenderer() {

        setOpaque(true);
        GUISettings.applyOrientation(this);
    }

    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        company = (Company) value;

        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }

        this.setText(company.getDescription());
        return this;
    }
}