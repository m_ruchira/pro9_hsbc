package com.isi.csvr.symbolselector;

import javax.swing.*;
import java.awt.*;
import java.util.EventObject;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class CompanyCellEditor extends DefaultCellEditor {

    private CellEditorComponent component;

    public CompanyCellEditor(JComboBox combo) {
        super(combo);
        component = new CellEditorComponent();
        addCellEditorListener(component);
    }

    public Object getCellEditorValue() {
        try {
            String symbol;
            symbol = component.getText().trim();
            return symbol;
        } catch (Exception ex) {
            return "";
        }
    }

    public Component getTableCellEditorComponent(JTable table, Object value,
                                                 boolean isSelected, int row, int column) {
        component.setEditor(this);
        component.clear();
        return component; //component;

    }

    public boolean isCellEditable(EventObject anEvent) {
        return super.isCellEditable(anEvent);
    }

    public boolean stopCellEditing() {
        //System.out.println("XX Stop");
        return super.stopCellEditing();

    }

    public void cancelCellEditing() {
        //System.out.println("XX Cancel");
        super.cancelCellEditing();
    }


}