package com.isi.csvr.table;

import java.awt.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public interface TableSettings {
    public Font getBodyFont();

    public Font getHeadingFont();

}