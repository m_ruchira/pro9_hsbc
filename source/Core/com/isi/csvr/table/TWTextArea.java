package com.isi.csvr.table;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyListener;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Feb 25, 2004
 * Time: 12:19:34 PM
 * To change this template use File | Settings | File Templates.
 */

public class TWTextArea extends JTextArea {

    public TWTextArea() {
    }

    public TWTextArea(String text) {
        super(text);
    }

    public void addKeyListener(KeyListener k) {
        super.addKeyListener(k);
    }

    public void selectAll() {
        super.selectAll();
    }

    public Object getItem() {
        return getText().trim();
    }

    public void setItem(Object anObject) {
        if (anObject != null)
            setText(anObject.toString());
        else
            setText("");
    }

    public Component getEditorComponent() {
        return this;
    }
}
