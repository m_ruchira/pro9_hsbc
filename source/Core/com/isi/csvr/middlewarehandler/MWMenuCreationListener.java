package com.isi.csvr.middlewarehandler;

/**
 * Created by IntelliJ IDEA.
 * User: janithk
 * Date: Mar 8, 2010
 * Time: 12:08:17 PM
 * To change this template use File | Settings | File Templates.
 */
public interface MWMenuCreationListener {

    public void mwTradeMenuCreated();

    public void mwServerDisconnected();

}
