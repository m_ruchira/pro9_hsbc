// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.iframe;

/**
 * A close button like icon for the infor frame close button
 * <P>
 * @author Uditha Nagahawatta
 */

import javax.swing.*;
import java.awt.*;

public class CloseButtonIcon implements Icon {

    private int ICON_SIZE = 16;


    public void paintIcon(Component c, Graphics g, int x, int y) {

        int iTop = 2;
        int iLeft = 2;
        g.setColor(UIManager.getColor("controlDkShadow"));

        /* X1 */
        g.drawLine(iLeft, iTop, ICON_SIZE - 1, iTop);
        g.drawLine(iLeft, iTop + 1, ICON_SIZE - 1, iTop + 1);

        /* X2 */
        g.drawLine(iLeft, ICON_SIZE - 2, ICON_SIZE - 1, ICON_SIZE - 2);
        g.setColor(UIManager.getColor("controlShadow"));
        g.drawLine(iLeft, ICON_SIZE - 1, ICON_SIZE - 1, ICON_SIZE - 1);

        /* Y1 */
        g.setColor(UIManager.getColor("controlDkShadow"));
        g.drawLine(iLeft, iTop, iLeft, ICON_SIZE - 1);
        g.drawLine(iLeft + 1, iTop, iLeft + 1, ICON_SIZE - 1);

        /* Y2 */
        g.drawLine(ICON_SIZE - 2, iTop, ICON_SIZE - 2, ICON_SIZE - 1);
        g.setColor(UIManager.getColor("controlShadow"));
        g.drawLine(ICON_SIZE - 1, iTop, ICON_SIZE - 1, ICON_SIZE - 1);

        g.setColor(UIManager.getColor("controlDkShadow"));
        g.drawLine(iLeft + 3, iTop + 3, ICON_SIZE - 4, ICON_SIZE - 4);

        g.drawLine(ICON_SIZE - 4, iTop + 3, iLeft + 3, ICON_SIZE - 4);

    }

    /**
     * Returns the width of the icon
     */
    public int getIconWidth() {
        return ICON_SIZE;
    }

    /**
     * Returns the height of the icon
     */
    public int getIconHeight() {
        return ICON_SIZE;
    }

}

