package com.isi.csvr.iframe;

import com.isi.csvr.TWMenuItem;

import javax.swing.*;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author unascribed
 * @version 1.0
 */

public class TWWindowMenuItem extends TWMenuItem {

    TWDesktopInterface item = null;

    public TWWindowMenuItem(TWDesktopInterface item) {
        super("");
        if ((item).getTitle().length() > 50)
            setText(((item).getTitle()).substring(0, 50) + "...");
        else
            setText((item).getTitle());
        this.item = item;

    }

    public String toString() {
        return (item).getTitle();
    }

    public JInternalFrame getFrame() {
        return (JInternalFrame) item;
    }
}