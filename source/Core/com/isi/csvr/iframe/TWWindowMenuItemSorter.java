package com.isi.csvr.iframe;

import java.util.Comparator;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author unascribed
 * @version 1.0
 */

public class TWWindowMenuItemSorter implements Comparator {

    public TWWindowMenuItemSorter() {
    }

    public int compare(Object o1, Object o2) {
        TWDesktopInterface i1 = (TWDesktopInterface) o1;
        TWDesktopInterface i2 = (TWDesktopInterface) o2;

        if (i1.getDesktopItemType() < i2.getDesktopItemType()) {
            return 1;
        } else if (i1.getDesktopItemType() > i2.getDesktopItemType()) {
            return -1;
        } else {
            return i1.getTitle().compareTo(i2.getTitle());
        }
    }

    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}