package com.isi.csvr.linkedwindows;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Oct 20, 2008
 * Time: 4:46:22 PM
 * To change this template use File | Settings | File Templates.
 */
public interface LinkedWindowListener {

    public void symbolChanged(String sKey);
}
