package com.isi.csvr;

import com.isi.csvr.announcement.Announcement;
import com.isi.csvr.brokers.Broker;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Market;
import com.isi.csvr.downloader.TodaysOHLCBacklogDownloadManager;
import com.isi.csvr.downloader.TodaysTradeBacklogDownloadManager;
import com.isi.csvr.event.Application;
import com.isi.csvr.forex.FXObject;
import com.isi.csvr.globalIndex.GlobalIndexObject;
import com.isi.csvr.ohlc.IntraDayOHLC;
import com.isi.csvr.shared.*;
import com.isi.csvr.ticker.SharedSettings;
import com.isi.csvr.ticker.advanced.*;
import com.isi.csvr.ticker.custom.TickerObject;
import com.isi.csvr.ticker.custom.TradeFeeder;
import com.isi.csvr.trade.Trade;
import com.isi.csvr.trade.TradeStore;
import com.isi.csvr.tradebacklog.TradeBacklogStore;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import static sun.java2d.pipe.BufferedOpCodes.SYNC;

public class TNSAramcoTickerFrameAnalyser extends Thread {
    private List<TNSTickerWrapper> tnsAramcoBuffer;
    private DataDisintegrator g_oDataDisintegrator;
    private String g_sTag;
    private int g_iTag;
    private String value;
    private Trade g_oTrade;
    public static long frameCount;
    private static final int BUFFERSIZE = 3000;

    public TNSAramcoTickerFrameAnalyser(List<TNSTickerWrapper> tnsAramcoBuffer) {
        super("TNSAramcoTickerFrameAnalyser");
        this.tnsAramcoBuffer = tnsAramcoBuffer;
        frameCount = 0;
    }

    public void run() {
        TNSTickerWrapper wrapper;
        String frame = null;
        StringBuffer stringBuffer = new StringBuffer();

        /* Take the authentication result first*/
        while ((tnsAramcoBuffer.size() == 0) && (Settings.isConnected())) {
            Thread.yield();
            Sleep(100);
        }

        g_oDataDisintegrator = new DataDisintegrator();
        g_oDataDisintegrator.setSeperator(Meta.DS);

        try {

            while (Settings.isConnected()) {
                try {
                    while (!tnsAramcoBuffer.isEmpty()) {
                        try {
                            wrapper = tnsAramcoBuffer.remove(0);
                            frame = wrapper.getMessage();
                            g_oTrade = wrapper.getG_oTrade();

                            if (tnsAramcoBuffer.size() > BUFFERSIZE) {
                                tnsAramcoBuffer.subList(tnsAramcoBuffer.size()/3,(tnsAramcoBuffer.size()-1)).clear();
                            }

                            if (frame != null) {
                                analyseTNS(frame);
                            }
                            frame = null;
                            frameCount++;
                        } catch (NoSuchElementException e) {
                            System.out.println("Frame Analyser Read Error " + e.toString());
                            if (!Settings.isTCPMode()) {
                                tnsAramcoBuffer.clear();
                            }
                            Sleep(500); //Bug ID <#0003>`
                            // this is a stupid javabug. this error should not raise since we check the size first
                        }
                    }
                    Sleep(500);
                } catch (Exception e) {
                    Sleep(1000);
                }
            }
            System.out.println("Frame Analyser Disconnected");
        } catch (Exception e) {
            System.out.println("Error :" + e);
        }
    }

    /**
     * Analyse the given frame for their tags.
     */
    private void analyseTNS(String sFrame) {
        try {
            String symbol = null;
            String symbolCode = null;
            String exchange = null;
            String exchangeCode = null;
            short messageType = -1;
            int instrumentType = -1;
            int decimalFactor = 1;
            double g_dLastTradePrice = 0;
            BidAsk oBid = null;
            BidAsk oAsk = null;
            Exchange exchangeObject;
            Market subMarket;
            Broker broker;
            IntraDayOHLC ohlcRecord = null;
            Stock oStock = null;
            GlobalIndexObject oGIndex = null;
            FXObject fxStock = null;
            boolean snapshotFrame = false;
            boolean backlogFrame = false;
//            boolean fullFrame = false;
            String marketCenter = null;

            int iDepth;

            StringTokenizer oRecords = new StringTokenizer(sFrame, Meta.RS);
            while (oRecords.hasMoreElements()) {

                iDepth = 0;
                oBid = null;
                oAsk = null;

                String rs = oRecords.nextToken();
                messageType = -1;
                instrumentType = -1;
                decimalFactor = 1;
                g_dLastTradePrice = 0;
                StringTokenizer oFields = new StringTokenizer(rs, Meta.FS);
                symbol = null;
                symbolCode = null;
                exchange = null;
                exchangeCode = null;
                oStock = null;
                exchangeObject = null;
                subMarket = null;
                broker = null;
                ohlcRecord = null;
                oGIndex = null;
                fxStock = null;
                Announcement oAnnouncement = null;
                snapshotFrame = false;
                backlogFrame = false;
                snapshotFrame = false;
                backlogFrame = false;
                marketCenter = null;
                //int priceCorrectionFactor;
//                fullFrame = false;

                while (oFields.hasMoreElements()) {
                    try {
                        g_oDataDisintegrator.setData(oFields.nextToken());
                        g_sTag = g_oDataDisintegrator.getTag();

                        /* If the tag is null, information is invalid */
                        if (g_sTag == null) continue;

                        g_iTag = Integer.parseInt(g_sTag);
                        value = g_oDataDisintegrator.getData();
                        switch (g_iTag) {
                            case Meta.FULL_FRAME:
                                snapshotFrame = (value.equals("1"));
//                                fullFrame = true;
                                break;
                            case Meta.MESSAGE_TYPE:
                                messageType = toShort(value);
                                break;
                            case Meta.SYMBOL:
                                symbol = value;
                                break;
                            case Meta.SYMBOL_CODE:
                                symbolCode = value;
                                break;
                            case Meta.EXCHANGE:
                                exchange = value;
                                break;
                            case Meta.EXCHANGE_CODE:
                                exchangeCode = value;
                                break;
                            case Meta.INSTRUMENT_TYPE:
                                instrumentType = toInt(value);
                                break;
                            case Meta.DECIMAL_CORRECTION_FACTOR:
                                decimalFactor = toInt(value);
                                break;
                            case Meta.MARKET_CENTER:
                                marketCenter = value;
                                break;
                            case Meta.FRAME_DATA:
                                exchange = getExchange(exchange, exchangeCode);
                                symbol = getSymbol(exchange, symbol, symbolCode);
//
                                break;


                            case Meta.USER_VERSION:
                                break;
                            case Meta.SNAPSHOT_FRAME:
                                snapshotFrame = true;
                                break;
                            case Meta.BACKLOG_FRAME:
                                backlogFrame = true;
                                break;
                            case Meta.PULSE:
                                Client.setMarketTime(value);
                                break;
                            case Meta.USER_SESSION:
                                Settings.setSessionID(value);
                                break;
                            case Meta.USERID:
                                Settings.setUserID(value);
                                break;
                            case Meta.SNAPSHOT_END:
                                processEndOfShapShot(value);
                                break;
                            case Meta.DAILY_TRADES: // timeand sales history, file list
                                TradeBacklogStore.getSharedInstance().setData(value);
                                break;
                            case Meta.EXCHANGE_TIME_REQUEST:
//                                System.out.println("Data Time = "+ value);
                                LatancyIndicator.getSharedInstance().setData(value);
                                break;
                            case Meta.EXCHANGE_TIME_LAG_THRESHOLD_REQUEST:
//                                System.out.println("Threshold Time = "+ value);
                                LatancyIndicator.getSharedInstance().setThreshold(value);
                                break;
                        }
                    } catch (Exception e) {
                        System.out.println("Error > " + sFrame);
                        e.printStackTrace();
                    }

                }
                rs = null;

                if ((symbol != null) && (exchange != null)) {
                    switch (messageType) {

                        case Meta.MESSAGE_TYPE_COMBINED_TRADE:
//                            if(exchange.equals("TDWL"))
//                                System.out.println("Trade = "+symbol + " , "+g_oTrade.getTradePrice());
/*
                                isTradeEnabledMode() zippedFrame  AddToTrades
                                        0                   0           1
                                        0                   1           0
                                        1                   0           1
                                        1                   1           1
                                */
                            // if (Settings.isTradeEnabledMode() || (!zippedFrame)) {
                            if ((ExchangeStore.getSharedInstance().getExchange(exchange)).isTimensalesEnabled()) {

                                boolean bVaidTrade = TradeStore.addTrade(g_oTrade, backlogFrame);
                                if (bVaidTrade) {
                                    g_oTrade.setSnapShotUpdatedTime(System.currentTimeMillis());
                                }
//                                TradeStore.addTrade((Trade)g_oTrade.clone(), backlogFrame);
//                                TradeStore.addTrade((Trade)g_oTrade.clone(), backlogFrame);
//                                TradeStore.addTrade((Trade)g_oTrade.clone(), backlogFrame);
                                /*if (bVaidTrade) {// && (Settings.isTradeEnabledMode())) {
                                    OHLCStore.getInstance().addTrade(g_oTrade);
                                }*/

                                if (SharedSettings.IS_CUSTOM_SELECTED) {
                                    if (ExchangeStore.isValidIinformationType(exchange, Meta.IT_TradeTicker)) {   //
                                        if (ExchangeStore.isValidIinformationType(exchange, Meta.IT_MarketTimeAndSales)) {
//                                            if ((ExchangeStore.getSelectedExchangeID() != null) && ((ExchangeStore.getSelectedExchangeID().equals(exchange)) || TradeFeeder.getMode() == TWTypes.TickerFilter.MODE_WATCHLIST)) {
                                            if ((ExchangeStore.getSelectedExchangeID() != null) && (ExchangeStore.getSelectedExchangeID().equals(exchange))) {
                                                if ((!snapshotFrame) && (!backlogFrame) && (!Settings.isShowSummaryTicker())) {
                                                    //if (Settings.isTradeEnabledMode() && (!Settings.isShowSummaryTicker())) {
                                                    if ((!g_oTrade.isIndexType()) && (bVaidTrade) && TradeFeeder.isVisible()) {
                                                        //if (TradeFeeder.isPreferredExchange(exchange)) {
                                                        TickerObject oTickerData = new TickerObject();
                                                        int tickerStatus;
                                                        if ((g_oTrade != null)) {
                                                            if (g_oTrade.getOddlot() == 1)
                                                                tickerStatus = Constants.TICKER_STATUS_SMALLTRADE;
                                                            else if (g_oTrade.getNetChange() == 0)
                                                                tickerStatus = Constants.TICKER_STATUS_NOCHANGE;
                                                            else if (g_oTrade.getNetChange() > 0)
                                                                tickerStatus = Constants.TICKER_STATUS_UP;
                                                            else
                                                                tickerStatus = Constants.TICKER_STATUS_DOWN;

                                                            oTickerData.setData(exchange, symbol, g_oTrade.getTradePrice(),
                                                                    g_oTrade.getTradeQuantity(), g_oTrade.getNetChange(),
                                                                    g_oTrade.getPrecentNetChange(), tickerStatus, g_oTrade.getSplits(), instrumentType);//
                                                            TradeFeeder.addData(oTickerData);

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if ((!snapshotFrame) && (!backlogFrame) && ((!g_oTrade.isIndexType()) && (bVaidTrade))) {

                                        ArrayList<String> upperexchangeList = UpperPanelSettings.getExchangeList();
                                        if (upperexchangeList.contains(exchange)) /*|| UpperFilterPanel.mode == TWTypes.TickerFilter.MODE_WATCHLIST)*/ {
                                            if (ExchangeStore.isValidIinformationType(exchange, Meta.IT_TradeTicker)) {
                                                if (ExchangeStore.isValidIinformationType(exchange, Meta.IT_MarketTimeAndSales)) {
                                                    if ((!UpperPanelSettings.showSummaryTicker) && UpperTickerFeeder.isVisible()) {
                                                        if (UpperTickerFeeder.isExchangeModeSelected || UpperTickerFeeder.isWatchListModeSelected) {
                                                            CommonTickerObject upperTickerOb = new CommonTickerObject();
                                                            upperTickerOb.setData(exchange, symbol, g_oTrade.getTradePrice(),
                                                                    g_oTrade.getTradeQuantity(), g_oTrade.getNetChange(),
                                                                    g_oTrade.getPrecentNetChange(), 0
                                                                    , g_oTrade.getSplits(), instrumentType);//
                                                            UpperTickerFeeder.addData(upperTickerOb);
                                                        }
                                                    }
                                                }
                                            }
                                        }


                                        ArrayList<String> middleexchangeList = MiddlePanelSettings.getExchangeList();
                                        if (middleexchangeList.contains(exchange)) /*|| MiddleFilterPanel.mode == TWTypes.TickerFilter.MODE_WATCHLIST)*/ {
                                            if (ExchangeStore.isValidIinformationType(exchange, Meta.IT_TradeTicker)) {
                                                if (ExchangeStore.isValidIinformationType(exchange, Meta.IT_MarketTimeAndSales)) {
                                                    if ((!MiddlePanelSettings.showSummaryTicker) && MiddleTickerFeeder.isVisible()) {
                                                        if (MiddleTickerFeeder.isExchangeModeSelected || MiddleTickerFeeder.isWatchListModeSelected) {
                                                            CommonTickerObject upperTickerOb = new CommonTickerObject();
                                                            upperTickerOb.setData(exchange, symbol, g_oTrade.getTradePrice(),
                                                                    g_oTrade.getTradeQuantity(), g_oTrade.getNetChange(),
                                                                    g_oTrade.getPrecentNetChange(), 0
                                                                    , g_oTrade.getSplits(), instrumentType);//
                                                            MiddleTickerFeeder.addData(upperTickerOb);
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        ArrayList<String> lowerexchangeList = LowerPanelSettings.getExchangeList();
                                        if (lowerexchangeList.contains(exchange))/*|| LowerFilterPanel.mode == TWTypes.TickerFilter.MODE_WATCHLIST)*/ {
                                            if (ExchangeStore.isValidIinformationType(exchange, Meta.IT_TradeTicker)) {
                                                if (ExchangeStore.isValidIinformationType(exchange, Meta.IT_MarketTimeAndSales)) {
                                                    if ((!LowerPanelSettings.showSummaryTicker) && LowerTickerFeeder.isVisible()) {
                                                        if (LowerTickerFeeder.isExchangeModeSelected || LowerTickerFeeder.isWatchListModeSelected) {
                                                            CommonTickerObject upperTickerOb = new CommonTickerObject();
                                                            upperTickerOb.setData(exchange, symbol, g_oTrade.getTradePrice(),
                                                                    g_oTrade.getTradeQuantity(), g_oTrade.getNetChange(),
                                                                    g_oTrade.getPrecentNetChange(), 0
                                                                    , g_oTrade.getSplits(), instrumentType);//
                                                            LowerTickerFeeder.addData(upperTickerOb);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                break;
                            }


                    }
                }
                oFields = null;
                oStock = null;
            }
        } catch (Exception e) {
            System.out.println("Frame Error > " + sFrame);
            e.printStackTrace();
        }
    }

    /**
     * Converts the given string to a long value
     */
    private long toLong(String sValue) {
        try {
            return Long.parseLong(sValue);
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * Converts the given string to an int value
     */
    private int toInt(String sValue) {
        try {
            return Integer.parseInt(sValue);
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * Converts the given string to a short value
     */
    private short toShort(String sValue) throws Exception {

        return Short.parseShort(sValue);
    }

    /**
     * Sleeps the threag for a given delay
     */
    private void Sleep(long lDelay) {
        try {
            sleep(lDelay);
        } catch (Exception e) {
        }
    }

    private String getSymbol(String exchange, String symbol, String code) {
        if ((symbol != null) && (code != null)) {
            SymbolMap.getSharedInstance().setMapData(exchange, code, symbol);
        } else if ((symbol == null) && (code != null)) {
            symbol = SymbolMap.getSharedInstance().getSymbolFor(exchange, code);
        }
        return symbol;
    }

    private String getExchange(String exchange, String code) {
        if ((exchange != null) && (code != null)) {
            ExchangeMap.setMapData(code, exchange);
        } else if ((exchange == null) && (code != null)) {
            exchange = ExchangeMap.getSymbolFor(code);
        }
        return exchange;
    }

    private void processEndOfShapShot(String exchangeSymbol) {
        System.out.println("Snapshot Received for " + exchangeSymbol);
        Exchange exchange = ExchangeStore.getSharedInstance().getExchange(exchangeSymbol);
        if ((exchange.isValidIinformationType(Meta.IT_SymbolTimeAndSales)) && (exchange.isValidIinformationType(Meta.IT_TimeAndSalesBacklog))) {
            TodaysTradeBacklogDownloadManager.getSharedInstance().startNewDownloadSession(value);
        }
//        } else {
        TodaysOHLCBacklogDownloadManager.getSharedInstance().startNewDownloadSession(exchange.getSymbol());
//        }
        Application.getInstance().fireSnapshotEnded(exchange);
        exchange = null;
    }
}
