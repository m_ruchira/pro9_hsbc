package com.isi.csvr.reports;

import com.isi.csvr.shared.Stock;

import java.util.Comparator;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class SectorComparator implements Comparator {

    public SectorComparator() {
    }

    public int compare(Object o1, Object o2) {
        Stock obj1 = (Stock) o1;
        Stock obj2 = (Stock) o2;

        return (obj1.getSectorCode().compareTo(obj2.getSectorCode()));
    }

//    public boolean equals(Object obj){
//        return super.equals(obj);
//    }

}