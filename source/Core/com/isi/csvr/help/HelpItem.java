package com.isi.csvr.help;

/**
 * Created by IntelliJ IDEA.
 * User: janithk
 * Date: Nov 4, 2009
 * Time: 2:26:30 PM
 * To change this template use File | Settings | File Templates.
 */
public class HelpItem {
    private String id;
    private String fileName;
    private String menuLabel;
    private boolean isDisplayed;
    private String imagePath;
    private int accrId;
    private boolean isExternalLink;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getMenuLabel() {
        return menuLabel;
    }

    public void setMenuLabel(String menuLabel) {
        this.menuLabel = menuLabel;
    }

    public boolean isExternalLink() {
        return isExternalLink;
    }

    public void setExternalLink(boolean externalLink) {
        isExternalLink = externalLink;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isDisplayed() {
        return isDisplayed;
    }

    public void setDisplayed(boolean displayed) {
        isDisplayed = displayed;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public int getAccrId() {
        return accrId;
    }

    public void setAccrId(int accrId) {
        this.accrId = accrId;
    }
}
