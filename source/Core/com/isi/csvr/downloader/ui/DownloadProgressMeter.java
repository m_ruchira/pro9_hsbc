package com.isi.csvr.downloader.ui;

import com.isi.csvr.event.ProgressListener;
import com.isi.csvr.shared.SharedMethods;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 20, 2005
 * Time: 11:04:05 AM
 */
public class DownloadProgressMeter implements ProgressListener {
    private String exchange;
    private String caption;
    private int progress;
    private int max;

    public DownloadProgressMeter(String exchange, String caption) {
        this.caption = caption;
        this.exchange = exchange;
        progress = 0;
    }

    public String getCaption() {
        return caption;
    }

    public String getExchange() {
        return exchange;
    }

    public int getProgress() {
        return progress;
    }

    public void setMax(String id, int max) {
        this.max = max / 1024;
    }

    public void setProcessCompleted(String id, boolean sucess) {
        progress = 100;
    }

    public void setProcessStarted(String id) {
        progress = 0;
    }

    public void setProgress(String id, int progress) {
        this.progress = progress;
    }


    public String toString() {
        return "<tr><td width=\"65\"><font face='Arial' size=3>&nbsp;" +
                exchange + "</font></td><td width=\"100\"><font face='Arial' size=3>" +
                caption + "</font></td><td width=\"40\" align=right><font face='Arial' size=3>" +
                SharedMethods.toIntegerFoamat(max) + "k</font></td><td width=\"60\" align=right><font face='Arial' size=3>" +
                progress + "%</font></td></tr>";
    }
}
