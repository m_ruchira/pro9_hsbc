package com.isi.csvr.downloader;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Dec 11, 2007
 * Time: 5:31:51 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ContentListener {

    public void contentDownloaded(boolean status, int type);
}
