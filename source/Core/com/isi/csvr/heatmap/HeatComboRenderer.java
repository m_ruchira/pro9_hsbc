package com.isi.csvr.heatmap;

import javax.swing.*;
import java.awt.*;


/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author unascribed
 * @version 1.0
 */

public class HeatComboRenderer extends JLabel
        implements ListCellRenderer {

    public HeatComboRenderer() {
        setOpaque(true);
    }


    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {

        HeatColorComboItem item = (HeatColorComboItem) value;
        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }
        Icon ico = item.getImage();
        setIcon(ico);
        setHorizontalAlignment(JLabel.CENTER);
        setPreferredSize(new Dimension(ico.getIconWidth() + 4, ico.getIconHeight() + 4));

        return this;
    }

}