package com.isi.csvr.globalMarket;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Jun 16, 2008
 * Time: 1:23:11 PM
 * To change this template use File | Settings | File Templates.
 */
public interface GlobalMarket {


    public int getRenderingID(int iRow, int iCol);

    public void removeRequestsOnClose();
}
