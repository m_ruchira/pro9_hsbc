package com.isi.csvr.topstocks;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Dec 31, 2007
 * Time: 12:51:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class TopStockInstrumentType {
    private String exchange;
    private String market;
    private byte instrumentType;
    private String descryption;

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public byte getInstrumentType() {
        return instrumentType;
    }

    public void setInstrumentType(byte instrumentType) {
        this.instrumentType = instrumentType;
    }

    public String getDescryption() {
        return descryption;
    }

    public void setDescryption(String descryption) {
        this.descryption = descryption;
    }
}
