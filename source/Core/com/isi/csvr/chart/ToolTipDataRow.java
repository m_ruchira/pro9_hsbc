package com.isi.csvr.chart;

/**
 * Created by IntelliJ IDEA.
 * User: charithn
 * Date: Sep 29, 2008
 * Time: 5:05:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class ToolTipDataRow {

    private String name;
    private String value;

    public ToolTipDataRow(String dataName, String dataVal) {
        this.name = dataName;
        this.value = dataVal;
    }

    public String getDataRowName() {
        return name;
    }

    public void setDataRowName(String name) {
        this.name = name;
    }

    public String getDataRowValue() {
        return value;
    }

    public void setDataRowValue(String value) {
        this.value = value;
    }
}
