package com.isi.csvr.chart.analysistechniques;

import com.isi.csvr.shared.Settings;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Sep 8, 2008
 * Time: 4:03:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class ChartConstants {

    public static final String CHART_ANALYSIS_INDICATOR_TEMP_PATH = Settings.CHART_DATA_PATH + "/tempAti.xml";

    //todo  need 3 encryption keys for each file
    public static final String ENCRYPT_FILE_PATH = Settings.CHART_DATA_PATH + "/atekXml.dll";

    public static final String ROOT_ELEMENT_PATTERN = "Pattern";
    public static final String ROOT_ELEMENT_STRATEGY = "Strategy";
    public static final String ROOT_ELEMENT_INDICATOR = "Indicator";
    public static final String ROOT_ELEMENT_CUSTOM_INDICATOR = "CustomIndicator";

    public static final String INDICATOR_ENCRYPTED_FILE = Settings.CHART_DATA_PATH + "/eiXml.dll";
    public static final String STRATEGY_ENCRYPTED_FILE = Settings.CHART_DATA_PATH + "/esXml.dll";
    public static final String PATTERN_ENCRYPTED_FILE = Settings.CHART_DATA_PATH + "/epXml.dll";
    public static final String CUST_IND_ENCRYPTED_FILE = Settings.CHART_DATA_PATH + "/ecIXml.dll";

    public static final String INDICATOR_FAVOURITE_FILE = Settings.CHART_DATA_PATH + "/ind_fav.dll";
    public static final String STRATEGY_FAVOURITE_FILE = Settings.CHART_DATA_PATH + "/stat_fav.dll";
    public static final String PATTERN_FAVOURITE_FILE = Settings.CHART_DATA_PATH + "/pat_fav.dll";
    public static final String CI_FAVOURITE_FILE = Settings.CHART_DATA_PATH + "/ci_fav.dll";

    public static final int INDICATOR = 0;
    public static final int PATTERN = 1;
    public static final int STRATEGY = 2;

    public static final int TYPE_INDICATOR = 0;
    public static final int TYPE_PATTERN = 1;
    public static final int TYPE_STRATEGY = 2;
    public static final int TYPE_CUSTOM = 3;
}
