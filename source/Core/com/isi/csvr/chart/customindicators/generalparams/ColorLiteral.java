package com.isi.csvr.chart.customindicators.generalparams;

/**
 * Created by IntelliJ IDEA.
 * User: udaka
 * Date: Mar 24, 2008
 * Time: 5:44:24 PM
 * To change this template use File | Settings | File Templates.
 */
public enum ColorLiteral {
    BLACK,
    BLUE,
    NAVY,
    GRAY,
    SILVER,
    RED,
    GREEN,
    YELLOW,
    PINK,
    ORANGE,
    BROWN,
    WHITE,
    OLIVE,
    CYAN,
    PURPLE,
    MAGENTA,
    LIME,
    MAROON
}
