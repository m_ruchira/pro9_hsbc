package com.isi.csvr.chart.customindicators;

/**
 * Created by IntelliJ IDEA.
 * User: udaka
 * Date: Feb 5, 2007
 * Time: 10:04:13 AM
 * To change this template use File | Settings | File Templates.
 */
public enum StatementType {
    PLOT,
    EMPTY,
    DECLARE_VAR,
    OBJECT_ASSIGN,
    FOR_DO_END,
    WHILE_DO_END,
    IF_THEN_END,
    IF_THEN_ELSE_END,
    DECLARE_ASSIGN
}
