package com.isi.csvr.chart.customindicators;

/**
 * Created by IntelliJ IDEA.
 * User: udaka
 * Date: Feb 4, 2007
 * Time: 5:33:22 PM
 * To change this template use File | Settings | File Templates.
 */
public enum ParamType {
    UNKNOWN,
    DOUBLE,
    INT,
    BOOL,
    DATA_ARRAY,
    MA_METHOD,
    CALC_METHOD,
    CC_PERIOD
}
