package com.isi.csvr.chart.customindicators;

/**
 * <p>Title: Mubasher Pro Chart</p>
 * <p>Company: Mubasher</p>
 * <p>Copyright: Mubasher (c) 2007</p>
 * Author: udaka
 * Date: Feb 14, 2007
 * Time: 12:40:25 AM
 */
public interface CustomIndicatorListener {

    void customIndicatorCreated(String name, Class indicatorClass, String grammer);

    void customIndicatorDeleted(String name);

    void strategyCreated(String name, Class indicatorClass);

    void patternCreated(String name, Class indicatorClass);
}
