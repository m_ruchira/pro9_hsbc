package com.isi.csvr.chart.customindicators;

/**
 * User: udaka
 * Date: Feb 5, 2007
 * Time: 10:11:11 AM
 */
public enum DeclarationType {
    VAR,
    PARAM
}
