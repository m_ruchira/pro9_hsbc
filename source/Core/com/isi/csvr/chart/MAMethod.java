package com.isi.csvr.chart;

/**
 * User: Pramoda
 * Date: Mar 15, 2007
 * Time: 2:50:14 PM
 */
public enum MAMethod {
    SIMPLE, WEIGHTED, EXPONENTIAL, VARIABLE, TIME_SERIES, TRIANGULAR, VOLUME_ADJUSTED
}
