package com.isi.csvr.chart;

import com.isi.csvr.shared.*;
import com.isi.csvr.*;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.win32.NativeMethods;
import com.isi.csvr.chart.customindicators.*;
import com.isi.csvr.chart.customindicators.statements.CIProgram;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.text.*;
import javax.swing.border.Border;
import java.awt.event.*;
import java.awt.*;
import java.awt.datatransfer.*;
import java.util.StringTokenizer;
import java.util.Vector;
import java.io.*;

/**
 * User: Pramoda
 * Date: Jan 22, 2007
 * Time: 3:00:09 PM
 */
public class IndicatorBuilder extends InternalFrame implements Themeable, NonResizeable {

    private static IndicatorBuilder thisInstance;

    // to communicate the results to main form
    private Vector<CustomIndicatorListener> ciListeners = null;

    private IndicatorBuilderPanel builderPanel = null;

    public IndicatorBuilder() {
        super();
        setTitle(Language.getString("INDICATOR_BUILDER_TITLE"));
        setSize(620, 476);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setResizable(true);
        setMaximizable(true);
        setIconifiable(true);
        setClosable(true);
        getContentPane().setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
        hideTitleBarMenu();

        ciListeners = new Vector<CustomIndicatorListener>();
        builderPanel = new IndicatorBuilderPanel(this);
        getContentPane().add(builderPanel);
        GUISettings.applyOrientation(this);
        GUISettings.applyOrientation(builderPanel.getScrollPane(), ComponentOrientation.LEFT_TO_RIGHT);        
    }

/*
    private void setHideButton() {
        divider = ((BasicSplitPaneUI) centerPanel.getUI()).getDivider();
        if (divider != null) {
            divider.setLayout(new BorderLayout(2, 2));
            divider.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            JPanel panel = new JPanel(new FlowLayout(FlowLayout.TRAILING, 5, 2));
            panel.add(btnErrorPanelShow);
            divider.add(panel, BorderLayout.CENTER);
            GUISettings.applyOrientation(divider);
        }
    }
*/

    public void addCustomIndicatorListener(CustomIndicatorListener cil) {
        this.ciListeners.add(cil);
    }

    public void removeCustomIndicatorListener(CustomIndicatorListener cil) {
        this.ciListeners.remove(cil);
    }

    public void showWindow() {
        //enableButtons(); // TODO:
        builderPanel.enableButtons();
        //Client.getInstance().getDesktop().add(this);
        ChartFrame.getSharedInstance().getDesktop().add(this);
        setLocationRelativeTo(ChartFrame.getSharedInstance().getDesktop());
        //Client.getInstance().getDesktop().setLayer(this, GUISettings.INTERNAL_DIALOG_LAYER);
        ChartFrame.getSharedInstance().getDesktop().setLayer(this, GUISettings.INTERNAL_DIALOG_LAYER);
        setVisible(true);
    }

    public void internalFrameClosing(InternalFrameEvent e) {
        super.internalFrameClosing(e);
        if (builderPanel.isDirty()) {
            int option = JOptionPane.showConfirmDialog(this, Language.getString("CI_UNSAVED_WORK_CONFIRM_MESSAGE"),
                    Language.getString("CI_UNSAVED_WORK_CONFIRM_MESSAGE_TITLE"), JOptionPane.OK_CANCEL_OPTION);
            if (option == JOptionPane.CANCEL_OPTION || option == JOptionPane.CLOSED_OPTION) {
                return;
            }
        }

        builderPanel.setDirty(false);
        builderPanel.getIndicatorTextPane().setText("");
        //builderPanel.getTabbedPane().setVisible(false);

        builderPanel.clearMessages();

        if (builderPanel.getActionFrame() != null) builderPanel.getActionFrame().dispose();
        dispose();
    }

    public static IndicatorBuilder getInstance() {
        if (thisInstance == null) {
            thisInstance = new IndicatorBuilder();
        }

        thisInstance.builderPanel.txtIndicatorName.setText("New Custom Indicator");
        thisInstance.builderPanel.editingIndicatorName = "New Custom Indicator";
        return thisInstance;
    }


    public Vector<CustomIndicatorListener> getCiListeners() {
        return ciListeners;
    }

    public IndicatorBuilderPanel getIndicatorBuilderPanel() {
        return builderPanel;
    }

}