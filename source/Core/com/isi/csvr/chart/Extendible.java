package com.isi.csvr.chart;

/**
 * User: Udaka
 * Date: Mar 31, 2006
 * Time: 12:56:00 PM
 */
public interface Extendible {
    boolean isExtendedLeft();
    boolean isExtendedRight();
    void setExtendedLeft(boolean extendedLeft);
    void setExtendedRight(boolean extendedRight);
}
