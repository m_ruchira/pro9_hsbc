package com.isi.csvr.chart.indicatorproperties;

/**
 * Created by IntelliJ IDEA.
 * User: shashikaw
 * Date: Aug 3, 2009
 * Time: 10:33:40 AM
 * To change this template use File | Settings | File Templates.
 */
public class IndicatorDataItem {

    private String name;
    private Object value;

    public IndicatorDataItem(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public Object getValue() {
        return value;
    }
}
