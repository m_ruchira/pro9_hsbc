package com.isi.csvr.chart.radarscreen.engine;

import com.isi.csvr.scanner.Scans.ScanRecord;

import java.util.Comparator;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jul 11, 2009
 * Time: 7:36:04 AM
 * To change this template use File | Settings | File Templates.
 */
public class RadarTransactionPoint extends ScanRecord implements Comparator {

    private ScanRecord currentRecord = null;
    private ScanRecord nextRecord = null;
    private int transactionType = RadarConstants.LOGIC_TYPE_BUY;


    public RadarTransactionPoint(long time, int trnType) {
        super();
        this.Time = time;
        this.transactionType = trnType;
    }

    public int getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(int transactionType) {
        this.transactionType = transactionType;
    }

    public ScanRecord getCurrentRecord() {
        return currentRecord;
    }

    public void setCurrentRecord(ScanRecord currentRecord) {
        this.currentRecord = currentRecord;
    }

    public ScanRecord getNextRecord() {
        return nextRecord;
    }

    public void setNextRecord(ScanRecord nextRecord) {
        this.nextRecord = nextRecord;
    }

    public double getPrice() {
        if (nextRecord != null) {
            return nextRecord.getOpen();
        } else {
            return currentRecord.getClose();
        }
    }

    public int compare(Object x, Object y) {
        long t1 = ((ScanRecord) x).Time;
        long t2 = ((ScanRecord) y).Time;
        return (t1 > t2) ? 1 : (t1 < t2) ? -1 : 0;
    }


}
