package com.isi.csvr.chart.backtesting;

import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;

import javax.swing.table.TableModel;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Nov 30, 2008
 * Time: 10:35:28 AM
 * To change this template use File | Settings | File Templates.
 */
public class DetailedSystemModel extends CommonTable implements TableModel, CommonTableInterface {

    private ArrayList<DetailedSystemSummary> systemSummaries;

    public DetailedSystemModel(ArrayList<DetailedSystemSummary> sysmtemSummaries) {
        this.systemSummaries = sysmtemSummaries;
    }

    public int getRowCount() {
        return systemSummaries.size();
    }

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {

        DetailedSystemSummary sysSummary = systemSummaries.get(rowIndex);
        switch (columnIndex) {

            case -1:
                return sysSummary;

            case 0:
                return String.valueOf(sysSummary.getId());

            case 1:
                return sysSummary.getSystemName();

            case 2:
                return sysSummary.getAvgNetProfit();
            case 3:
                return sysSummary.getAvgGain();

            case 4:
                return sysSummary.getBestProfit();

            case 5:
                return sysSummary.getWorstProfit();

            case 6:
                return sysSummary.getAvgTrades();

            case 7:
                return sysSummary.getAvgTradesPL();

            case 8:
                return sysSummary.getAvgPL();

            default:
                return "";
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        return getValueAt(0, iCol).getClass();
    }

    public void setSymbol(String symbol) {

    }
}
