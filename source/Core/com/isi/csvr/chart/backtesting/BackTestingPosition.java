package com.isi.csvr.chart.backtesting;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Nov 18, 2008
 * Time: 1:14:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class BackTestingPosition {
    public int Count;
    public double BuyPrice;
    public double SellPrice;
    public int Quantity;
    public int BuyPos;
    public int SellPos;

    public BackTestingPosition(int Count, int BuyPos, double BuyPrice, int Quantity) {
        this.Count = Count;
        this.BuyPos = BuyPos;
        this.BuyPrice = BuyPrice;
        this.Quantity = Quantity;
    }

    public void setSellData(int SellPos, double SellPrice) {
        this.SellPrice = SellPrice;
        this.SellPos = SellPos;
    }
}
