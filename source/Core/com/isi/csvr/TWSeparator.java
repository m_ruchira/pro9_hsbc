package com.isi.csvr;


import javax.swing.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jan 11, 2005
 * Time: 9:58:24 AM
 */
public class TWSeparator extends JSeparator {

    private String caption;

    public TWSeparator(String caption) {
        super();
        this.caption = caption;
        setUI(new TWSeparatorUI(caption));
    }

    public TWSeparator() {
        super();
        this.caption = "";
        setUI(new TWSeparatorUI(""));
    }

    public void updateUI() {
        super.updateUI();
        setUI(new TWSeparatorUI(caption));
    }
}
