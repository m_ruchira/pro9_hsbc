package com.isi.csvr.performaceHive;

/**
 * Created by IntelliJ IDEA.
 * User: dilini
 * Date: Jul 30, 2009
 * Time: 11:34:59 AM
 * To change this template use File | Settings | File Templates.
 */

import java.util.Enumeration;
import java.util.Vector;
import javax.swing.tree.DefaultMutableTreeNode;

// Referenced classes of package net.sf.jtreemap.swing:
//            Value

public class TreeMapNode extends DefaultMutableTreeNode {

    private static int border = 3;
    private int height;
    private Value value;
    private double weight;
    private int width;
    private int x;
    private int y;
    private String symbol;
    private String lastPrice;
    private String percentChange;
    private String volume;
    private String lastQty;
    private String change;
    private String vWap;
    private String turnOver;
    private String trades;
    private String description;
    private String key;

    public static int getBorder() {
        return border;
    }

    public static void setBorder(int border) {
        border = border;
    }

    public TreeMapNode(String label) {
        super(label);
        weight = 0.0D;
        super.allowsChildren = true;
    }

    public TreeMapNode(String label, double weight, Value value, String symbol, String lastPrice, String percentChange, String volume, String lastQty, String change, String vWap, String turnOver, String trades, String description) {
        super(label);
        this.weight = 0.0D;
        if(!String.valueOf(weight).equals("NaN")){
            this.weight = Math.abs(weight);
        }
        this.value = value;
        this.key = label;
        this.symbol = symbol;
        this.lastPrice = lastPrice;
        this.percentChange = percentChange;
        this.volume = volume;
        super.allowsChildren = false;
        this.lastQty = lastQty;
        this.change = change;
        this.vWap = vWap;
        this.turnOver = turnOver;
        this.trades = trades;
        this.description = description;
    }

    public void add(TreeMapNode newChild) {
        super.add(newChild);
        setWeight(weight + newChild.getWeight());
    }

    public TreeMapNode getActiveLeaf(int xParam, int yParam) {
        label0:
        {
            if (isLeaf()) {
                if (xParam >= getX() && xParam <= getX() + getWidth() && yParam >= getY() && yParam <= getY() + getHeight())
                    return this;
                break label0;
            }
            Enumeration e = children();
            TreeMapNode node;
            do {
                if (!e.hasMoreElements())
                    break label0;
                node = (TreeMapNode) e.nextElement();
            }
            while (xParam < node.getX() || xParam > node.getX() + node.getWidth() || yParam < node.getY() || yParam > node.getY() + node.getHeight());
            return node.getActiveLeaf(xParam, yParam);
        }
        return null;
    }

    public TreeMapNode getChild(int xParam, int yParam) {
        label0:
        {
            if (isLeaf())
                break label0;
            Enumeration e = children();
            TreeMapNode node;
            do {
                if (!e.hasMoreElements())
                    break label0;
                node = (TreeMapNode) e.nextElement();
            }
            while (xParam < node.getX() || xParam > node.getX() + node.getWidth() || yParam < node.getY() || yParam > node.getY() + node.getHeight());
            return node;
        }
        return null;
    }

    public Vector getChildren() {
        return children;
    }

    public int getHeight() {
        return height;
    }

    public String getLabel() {
        return getUserObject().toString();
    }

    public String getLabelValue() {
        return value.getLabel();
    }

    public Value getValue() {
        return value;
    }

    public double getDoubleValue() {
        return value.getValue();
    }

    public double getWeight() {
        return weight;
    }

    public int getWidth() {
        return width;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setDimension(int xParam, int yParam, int widthParam, int heightParam) {
        x = xParam;
        y = yParam;
        width = widthParam;
        height = heightParam;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setLabel(String label) {
        userObject = label;
    }

    public void setPosition(int xParam, int yParam) {
        x = xParam;
        y = yParam;
    }

    public void setSize(int widthParam, int heightParam) {
        width = widthParam;
        height = heightParam;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    public void setWeight(double weight) {
        double newWeight = Math.abs(weight);
        if (parent != null)
            ((TreeMapNode) parent).setWeight((((TreeMapNode) parent).weight - this.weight) + newWeight);
        this.weight = newWeight;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getLastPrice() {
        return lastPrice;
    }

    public String getPercentChange() {
        return percentChange;
    }

    public String getVolume() {
        return volume;
    }

    public String getLastQty() {
        return lastQty;
    }

    public String getDescription() {
        return description;
    }

    public String getChange() {
        return change;
    }

    public String getVWap() {
        return vWap;
    }

    public String getTurnOver() {
        return turnOver;
    }

    public String getTrades() {
        return trades;
    }

    public String getKey() {
        return key;
    }
}
