/**
 * Created by IntelliJ IDEA.
 * User: pradeeps
 * Date: Feb 26, 2008
 * Time: 5:22:55 PM
 * To change this template use File | Settings | File Templates.
 */

package com.isi.csvr.mchat.shared;

import java.util.Vector;

public class MChatConfirmAddData {
    public String tok = null;
    public Vector data = new Vector();
    public String requestData = null;

    public MChatConfirmAddData() {
    }

    public Vector getRequest() {
        try {
            data.clear();
            if (!this.getRequestData().equals("")) {
                MChatStringTokenizer tokFirst = new MChatStringTokenizer(this.getRequestData(), ",", false);
                while (tokFirst.hasMoreElements()) {
                    String name = (String) tokFirst.nextElement();
                    String chatID = (String) tokFirst.nextElement();
                    String fName = (String) tokFirst.nextElement();
                    String lName = (String) tokFirst.nextElement();
                    String invite = null;
                    if (tokFirst.hasMoreElements()) {
                        invite = (String) tokFirst.nextElement();
                    }
                    data.addElement(name);
                    data.addElement(chatID);
                    data.addElement(fName);
                    data.addElement(lName);
                    if (invite != null) {
                        data.addElement(invite);
                    } else {
                        data.addElement(MChatLanguage.getString("HI"));
                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException("Request string is not in right format!");
        }
        return data;
    }

    public String getRequestData() {
        return requestData;
    }

    public void setRequestData(String data) {
        requestData = data;
    }
}
