package com.isi.csvr;

import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Feb 23, 2004
 * Time: 11:43:41 AM
 * To change this template use File | Settings | File Templates.
 */
public class SymbolMap {
    public static SymbolMap self = null;

    private Hashtable map;

    private SymbolMap() {
        map = new Hashtable();
    }

    public static synchronized SymbolMap getSharedInstance() {
        if (self == null) {
            self = new SymbolMap();
        }
        return self;
    }

    public void init() {
        map.clear();
    }

    public String getSymbolFor(String exchange, String id) {
        Hashtable exchangetable = (Hashtable) map.get(exchange);
        if (exchangetable != null) {
            return (String) exchangetable.get(id);
        }
        return null;
    }

    public void setMapData(String exchange, String id, String symbol) {
        Hashtable exchangetable = (Hashtable) map.get(exchange);
        if (exchangetable == null) {
            exchangetable = new Hashtable();
            map.put(exchange, exchangetable);  // create a new exchange
        }
        exchangetable.put(id, symbol);
    }
}
