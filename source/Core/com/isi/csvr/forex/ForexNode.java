package com.isi.csvr.forex;

/**
 * Created by IntelliJ IDEA. User: admin Date: 12-Sep-2007 Time: 12:08:25 To change this template use File | Settings |
 * File Templates.
 */
public class ForexNode {

    protected int m_id;
    protected String m_name;

    public ForexNode(int id, String name) {
        m_id = id;
        m_name = name;
    }

    public int getId() {
        return m_id;
    }

    public String getName() {
        return m_name;
    }

    public String toString() {
        return m_name;
    }

}
