package com.isi.csvr.customindex;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Oct 16, 2006
 * Time: 8:09:16 PM
 * To change this template use File | Settings | File Templates.
 */
public interface CustomIndexInterface {

    String getSymbol();

    String getDescription();

    String getExchanges();

    String getDateString();

    long getDate();

    double getValue();

    int getType();

    double getPrice();

    double getPreviousClosed();

    String[] getSymbolsArray();

    double getLastPrice();

    double getCalculatedValue(double price);

    long getVolumn();

    long getNoOfTrades();

    double getTurnover();

    void setDataStore(com.isi.csvr.shared.FunctionDataStoreInterface datastore);

}
