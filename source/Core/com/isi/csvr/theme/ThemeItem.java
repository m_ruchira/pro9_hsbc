package com.isi.csvr.theme;

import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.FontUIResource;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 16, 2005
 * Time: 10:50:54 AM
 */
public class ThemeItem {
    public String setting;
    public Object value;

    public String toString() {
        if (value instanceof ColorUIResource) {
            return setting + "=" + ((Color) value).getRGB() + "\r\n";
        } else if (value instanceof FontUIResource) {
            return setting + "=" + ((Font) value).getFamily() + "," + ((Font) value).getStyle() + "," + ((Font) value).getSize() + "\r\n";
        } else {
            return setting + "=" + value.toString() + "\r\n";
        }
    }

}
