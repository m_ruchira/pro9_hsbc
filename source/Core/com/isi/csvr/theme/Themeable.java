package com.isi.csvr.theme;

public interface Themeable {
    public void applyTheme();
}