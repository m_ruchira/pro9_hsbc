package com.isi.csvr.TradeStation;

/**
 * Created by IntelliJ IDEA.
 * User: shashikaw
 * Date: Oct 18, 2010
 * Time: 4:11:19 PM
 * To change this template use File | Settings | File Templates.
 */

public class TradeStationHistoryRecord {
    public int date;
    public float open;
    public float high;
    public float low;
    public float close;
    public long volume;

    private String FS = ",";

    public TradeStationHistoryRecord(int date, float open, float high, float low, float close, long volume) {
        this.date = date;
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
        this.volume = volume;
    }

    public String getString() {
        return date + FS + open + FS + high + FS + low + FS + close + FS + volume + "\n";
    }
}
