package com.isi.csvr.TradeStation;


/**
 * Created by IntelliJ IDEA.
 * User: shashikaw
 * Date: Oct 21, 2010
 * Time: 10:36:09 AM
 * To change this template use File | Settings | File Templates.
 */
public class TradeStationOHLCRecord implements Comparable {
    private long time;
    private float open;
    private float high;
    private float low;
    private float close;
    private long volume;

    public TradeStationOHLCRecord(long time, float open, float high, float low, float close, long volume) {
        this.time = time;
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
        this.volume = volume;
    }

    public long getTime() {
        return time;
    }

    public float getOpen() {
        return open;
    }

    public float getHigh() {
        return high;
    }

    public float getLow() {
        return low;
    }

    public float getClose() {
        return close;
    }

    public long getVolume() {
        return volume;
    }

    public int compareTo(Object o) {
        long time = ((TradeStationOHLCRecord) o).getTime();
        if (this.time > time)
            return 1;
        else if (this.time < time)
            return -1;
        else
            return 0;
    }
}
