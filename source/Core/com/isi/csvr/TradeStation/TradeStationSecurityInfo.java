package com.isi.csvr.TradeStation;

/**
 * Created by IntelliJ IDEA.
 * User: shashikaw
 * Date: Oct 28, 2010
 * Time: 10:26:10 AM
 * To change this template use File | Settings | File Templates.
 */
public class TradeStationSecurityInfo {
    private String symbol;
    private String description;

    public TradeStationSecurityInfo(String symbol, String desc) {
        this.symbol = symbol;
        this.description = desc;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getDescription() {
        return description;
    }
}
