package com.isi.csvr.mutualfund;


/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Dec 12, 2003
 * Time: 9:32:09 AM
 * To change this template use Options | File Templates.
 */
public class MutualFund implements Comparable {
    private String fundName;
    private String manager;
    private String currency;
    private String category;
    private String subCategory;
    private String classification;
    private String unitPrice;
    private String ytdChange;
    private String valuationDate;

    public MutualFund(String fundName, String manager,
                      String currency, String category, String subCategory,
                      String classification, String unitPrice,
                      String ytdChange, String valuationDate) {
        this.category = category;
        this.classification = classification;
        this.currency = currency;
        this.fundName = fundName;
        this.manager = manager;
        this.subCategory = subCategory;
        this.unitPrice = unitPrice;
        this.valuationDate = valuationDate;
        this.ytdChange = ytdChange;
    }//Fund Name,Manager,Currency,Category,Sub Category,Classification,Unit Price,%YTD Chg.,Valuation Date

    public String getCategory() {
        return category;
    }

    public String getClassification() {
        return classification;
    }

    public String getCurrency() {
        return currency;
    }

    public String getFundName() {
        return fundName;
    }

    public String getManager() {
        return manager;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public String getValuationDate() {
        return valuationDate;
    }

    public String getYtdChange() {
        return ytdChange;
    }

    public int compareTo(Object obj) {
        MutualFund other = (MutualFund) obj;

        // sort by Manager, then Fund name
        if (manager.equals(other.getManager())) {
            return fundName.compareTo(other.getFundName());
        } else {
            return (manager.compareTo(other.getManager()));
        }
    }

}
