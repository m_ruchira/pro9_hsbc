package com.isi.csvr.mutualfund;

import javax.swing.*;
import javax.swing.event.ListDataListener;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Dec 15, 2003
 * Time: 10:19:04 AM
 * To change this template use Options | File Templates.
 */
public class MutualFundManagerModel extends DefaultComboBoxModel {
    public static final int MANAGER = 0;
    public static final int CURRENCY = 1;

    private int type;

    public MutualFundManagerModel(int type) {
        this.type = type;
    }

    public int getSize() {
        if (type == MANAGER)
            return MutualFundStore.getInstance().getManagerCount();
        else
            return MutualFundStore.getInstance().getCurrencyCount();
    }

    public Object getElementAt(int index) {
        if (type == MANAGER)
            return MutualFundStore.getInstance().getManager(index);
        else
            return MutualFundStore.getInstance().getCurrency(index);
    }

    public void addListDataListener(ListDataListener l) {
    }
}
