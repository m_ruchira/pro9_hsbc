package com.isi.csvr.mutualfund;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Dec 15, 2003
 * Time: 9:24:52 AM
 * To change this template use Options | File Templates.
 */
public class MutualFundField implements Comparable {
    private String description;

    public MutualFundField(String description) {
        this.description = description;
    }

    public String toString() {
        return description;
    }

    public int compareTo(Object o) {
        return description.compareTo(o.toString());
    }
}
