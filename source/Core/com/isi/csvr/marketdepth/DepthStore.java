package com.isi.csvr.marketdepth;

import com.isi.csvr.datastore.DataStoreInterface;
import com.isi.csvr.shared.BidAsk;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Stock;

import java.util.Enumeration;
import java.util.Hashtable;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class DepthStore implements DataStoreInterface {

    private Hashtable tree = null;
    private static DepthStore self = null;

    private DepthStore() {
        tree = new Hashtable();
    }

    public static synchronized DepthStore getInstance() {
        if (self == null) {
            self = new DepthStore();
        }
        return self;
    }

    public synchronized DepthObject getDepthFor(String symbol) {
        DepthObject depth = (DepthObject) tree.get(symbol);
        if (depth == null) {
            depth = new DepthObject();
            tree.put(symbol, depth);
        }
        return depth;
    }

    /*
     * Clear all depth objcts (initialize)
     */

    public void clear(String symbol) {
    }

    public void clear() {
        Enumeration depthObjects = tree.elements();

        while (depthObjects.hasMoreElements()) {
            DepthObject depthObject = (DepthObject) depthObjects.nextElement();
            depthObject.clear();
            depthObject = null;
        }
        depthObjects = null;
    }
    public String getXMLFor(String symbol,String depthMode){
        DepthObject depth = (DepthObject) tree.get(symbol);
        if (depth == null) {
            depth = new DepthObject();
            tree.put(symbol, depth);
        }
        return depth.getXML(symbol,depthMode);
    }

    public void setDepthFrame(int type, String key, String frame, int decimalFactor) {

        BidAsk oBid = null;
        BidAsk oAsk = null;

        String[] data = frame.split(Meta.FD);
        char tag;
        int depth = -1;

        for (int i = 0; i < data.length; i++) {
            tag = data[i].charAt(0);
            switch (tag) {
                case '4':
                    depth = toInt(data[i].substring(1));
                    if (type == Meta.MESSAGE_TYPE_DEPTH_PRICE) {
                        oBid = getPriceOrder(key, depth, DepthObject.BID);
                        oAsk = getPriceOrder(key, depth, DepthObject.ASK);
                    } else if (type == Meta.MESSAGE_TYPE_DEPTH_REGULAR) {
                        oBid = getRegularOrder(key, depth, DepthObject.BID);
                        oAsk = getRegularOrder(key, depth, DepthObject.ASK);
                    } else if (type == Meta.MESSAGE_TYPE_DEPTH_SPECIAL) {
                        oBid = getSpecialOrder(key, depth, DepthObject.BID);
                        oAsk = getSpecialOrder(key, depth, DepthObject.ASK);
                    }
                    oBid.setDepthSequance((depth));
                    oAsk.setDepthSequance((depth));
                    break;
                case '5':
                    if (oAsk != null)
                        oAsk.setPrice(SharedMethods.getDouble(data[i].substring(1)) / decimalFactor);
                    break;
                case '6':
                    if (oBid != null)
                        oBid.setPrice(SharedMethods.getDouble(data[i].substring(1)) / decimalFactor);
                    break;
                case '7':
                    if (oAsk != null)
                        oAsk.setPrice(SharedMethods.getDouble(data[i].substring(1)) / decimalFactor);
                    if (oBid != null)
                        oBid.setPrice(oAsk.getPrice());
                    break;
                case '8':
                    if (oAsk != null) {
                        oAsk.setQuantity(SharedMethods.getLong(data[i].substring(1)));
                        setDepthSize(key, oAsk.getQuantity(), depth, DepthObject.ASK, (short) type);
                    }
                    break;
                case '9':
                    if (oBid != null) {
                        oBid.setQuantity(SharedMethods.getLong(data[i].substring(1)));
                        setDepthSize(key, oBid.getQuantity(), depth, DepthObject.BID, (short) type);
                    }
                    break;
                case ':':
                    if (oAsk != null) {
                        oAsk.setQuantity(SharedMethods.getLong(data[i].substring(1)));
                        setDepthSize(key, oAsk.getQuantity(), depth, DepthObject.ASK, (short) type);
                    }
                    if (oBid != null) {
                        oBid.setQuantity(oAsk.getQuantity());
                        setDepthSize(key, oBid.getQuantity(), depth, DepthObject.BID, (short) type);
                    }
                    break;
                case ';':
                    if (oAsk != null)
                        oAsk.setSplits(toInt(data[i].substring(1)));
                    break;
                case '<':
                    if (oBid != null)
                        oBid.setSplits(toInt(data[i].substring(1)));
                    break;
                case '=':
                    oAsk.setSplits(toInt(data[i].substring(1)));
                    oBid.setSplits(oAsk.getSplits());
                    break;
                case Stock.BATE_DEPTH_ASK_ORDER_NO:
                    if (oAsk != null) {
                        oAsk.setOrderNo(toLong(data[i].substring(1)));
                    }
                    break;
                case Stock.BATE_DEPTH_BID_ORDER_NO:
                    if (oBid != null) {
                        oBid.setOrderNo(toLong(data[i].substring(1)));
                    }
                    break;
                case Stock.BATE_DEPTH_BIDASK_ORDER_NO:
                    if (oAsk != null) {
                        oAsk.setOrderNo(toLong(data[i].substring(1)));
                    }
                    if (oBid != null) {
                        oBid.setOrderNo(toLong(data[i].substring(1)));
                    }
                    break;

                case Stock.BATE_DEPTH_ASK_MKT_CODE:
                    if (oAsk != null) {
                        oAsk.setMarketCode(data[i].substring(1));
                    }
                    break;
                case Stock.BATE_DEPTH_BID_MKT_CODE:
                    if (oBid != null) {
                        oBid.setMarketCode(data[i].substring(1));
                    }
                    break;
                case Stock.BATE_DEPTH_BIDASK_MKT_CODE:
                    if (oAsk != null) {
                        oAsk.setMarketCode(data[i].substring(1));
                    }
                    if (oBid != null) {
                        oBid.setMarketCode(data[i].substring(1));
                    }
                    break;

                case Stock.BATE_DEPTH_ASK_FILL_FLAG:
                    if (oAsk != null) {
                        oAsk.setFILL_flags(toInt(data[i].substring(1)));
                    }
                    break;
                case Stock.BATE_DEPTH_BID_FILL_FLAG:
                    if (oBid != null) {
                        oBid.setFILL_flags(toInt(data[i].substring(1)));
                    }
                    break;
                case Stock.BATE_DEPTH_BIDASK_FILL_FLAG:
                    if (oAsk != null) {
                        oAsk.setFILL_flags(toInt(data[i].substring(1)));
                    }
                    if (oBid != null) {
                        oBid.setFILL_flags(toInt(data[i].substring(1)));
                    }
                    break;

                case Stock.BATE_DEPTH_ASK_TIFF_FLAG:
                    if (oAsk != null) {
                        oAsk.setTIF_flags(toInt(data[i].substring(1)));
                    }
                    break;
                case Stock.BATE_DEPTH_BID_TIFF_FLAG:
                    if (oBid != null) {
                        oBid.setTIF_flags(toInt(data[i].substring(1)));
                    }
                    break;
                case Stock.BATE_DEPTH_BIDASK_TIFF_FLAG:
                    if (oAsk != null) {
                        oAsk.setTIF_flags(toInt(data[i].substring(1)));
                    }
                    if (oBid != null) {
                        oBid.setTIF_flags(toInt(data[i].substring(1)));
                    }
                    break;

                case Stock.BATE_DEPTH_ASK_MIN_AMOUNT:
                    if (oAsk != null) {
                        oAsk.setReg_Time(SharedMethods.getLong(data[i].substring(1)));
                    }
                    break;
                case Stock.BATE_DEPTH_BID_MIN_AMOUNT:
                    if (oBid != null) {
                        oBid.setReg_Time(SharedMethods.getLong(data[i].substring(1)));
                    }
                    break;
                case Stock.BATE_DEPTH_BIDASK_MIN_AMOUNT:
                    if (oAsk != null) {
                        oAsk.setReg_Time(SharedMethods.getLong(data[i].substring(1)));
                    }
                    if (oBid != null) {
                        oBid.setReg_Time(SharedMethods.getLong(data[i].substring(1)));
                    }
                    break;
            }
        }
        oAsk = null;
        oBid = null;
    }

    /**
     * Extract the bid object fot the given depth
     */
    private BidAsk getPriceOrder(String symbol, int iDepth, byte type) {
        BidAsk bidask = null;
        try {
            bidask = ((BidAsk) DepthStore.getInstance().getDepthFor(symbol).getPriceList(type).get(iDepth));
            if (bidask == null) {
                throw new Exception("Not found");
            }
        } catch (Exception e) {
            bidask = new BidAsk(symbol);
            DepthStore.getInstance().getDepthFor(symbol).getPriceList(type).add(bidask);
        }
        return bidask;
    }

    private BidAsk getSpecialOrder(String symbol, int iDepth, byte type) {
        BidAsk bidask = null;
        try {
            bidask = ((BidAsk) DepthStore.getInstance().getDepthFor(symbol).getSpecialList(type).get(iDepth));
            if (bidask == null) {
                throw new Exception("Not found");
            }
        } catch (Exception e) {
            bidask = new BidAsk(symbol);
            DepthStore.getInstance().getDepthFor(symbol).getSpecialList(type).add(bidask);
        }
        return bidask;
    }


    private void setDepthSize(String symbol, long lQuantity, int depth, byte type, short shtSymbolType) {
        int currentSize = 0;
        switch (shtSymbolType) {
            case Meta.MESSAGE_TYPE_DEPTH_REGULAR:
                currentSize = DepthStore.getInstance().getDepthFor(symbol).getOrderListSize(type);
                break;
            case Meta.MESSAGE_TYPE_DEPTH_PRICE:
                currentSize = DepthStore.getInstance().getDepthFor(symbol).getPriceListSize(type);
                break;
            case Meta.MESSAGE_TYPE_DEPTH_SPECIAL:
                currentSize = DepthStore.getInstance().getDepthFor(symbol).getSpecialListSize(type);
        }

        if (lQuantity == 0) {
            if (currentSize >= (depth + 1)) {
                switch (shtSymbolType) {
                    case Meta.MESSAGE_TYPE_DEPTH_REGULAR:
                        DepthStore.getInstance().getDepthFor(symbol).setOrderListSize(type, depth);
                        break;
                    case Meta.MESSAGE_TYPE_DEPTH_PRICE:
                        DepthStore.getInstance().getDepthFor(symbol).setPriceListSize(type, depth);
                        break;
                    case Meta.MESSAGE_TYPE_DEPTH_SPECIAL:
                        DepthStore.getInstance().getDepthFor(symbol).setSpecialListSize(type, depth);
                }
            }
        } else {
            if (currentSize < (depth + 1)) {
                currentSize = (depth + 1);
                switch (shtSymbolType) {
                    case Meta.MESSAGE_TYPE_DEPTH_REGULAR:
                        DepthStore.getInstance().getDepthFor(symbol).setOrderListSize(type, currentSize);
                        break;
                    case Meta.MESSAGE_TYPE_DEPTH_PRICE:
                        DepthStore.getInstance().getDepthFor(symbol).setPriceListSize(type, currentSize);
                        break;
                    case Meta.MESSAGE_TYPE_DEPTH_SPECIAL:
                        DepthStore.getInstance().getDepthFor(symbol).setSpecialListSize(type, currentSize);
                }
            }
        }

    }

    /**
     * Extract the bid object fot the given depth
     */
    private BidAsk getRegularOrder(String symbol, int iDepth, byte type) {
        BidAsk bidask = null;
        try {
            //if (iDepth== -1) iDepth = 0;
            //return (BidAsk)oStock.getAsk(iDepth).clone();
            bidask = ((BidAsk) DepthStore.getInstance().getDepthFor(symbol).getOrderList(type).get(iDepth));
            if (bidask == null) {
                throw new Exception("Not found");
            }
        } catch (Exception e) {
            bidask = new BidAsk(symbol);
            //bidask.setDepthSequance((byte)iDepth);
            DepthStore.getInstance().getDepthFor(symbol).getOrderList(type).add(bidask);
        }
        return bidask;
    }

    /**
     * Converts the given string to a long value
     */
    private long toLong(String sValue) {
        try {
            return Long.parseLong(sValue);
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * Converts the given string to an int value
     */
    private int toInt(String sValue) {
        try {
            return Integer.parseInt(sValue);
        } catch (Exception e) {
            return 0;
        }
    }

}